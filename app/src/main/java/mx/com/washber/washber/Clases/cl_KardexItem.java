package mx.com.washber.washber.Clases;

/**
 * Created by ppcoo on 26/12/2017.
 */

public class cl_KardexItem {
    public String Titulo;
    public String Mensaje;
    public String Fecha;

    public cl_KardexItem(String titulo,String mensaje, String fecha)
    {
        this.Titulo = titulo;
        this.Mensaje = mensaje;
        this.Fecha = fecha;
    }
}
