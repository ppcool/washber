package mx.com.washber.washber.WebService;

import java.util.ArrayList;

/**
 * Created by ppcoo on 29/12/2017.
 */

public class ws_ProveedorCercaniaResult {
    public Integer IdProveedor;
    public String RazonSocial;
    public String Logotipo;
    public ArrayList<ws_SucursalCercaniaResult> Sucursales;
    public ArrayList<ws_CategoriaCercaniaResult> Categorias;
}
