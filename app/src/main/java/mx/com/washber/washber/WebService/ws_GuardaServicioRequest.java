package mx.com.washber.washber.WebService;

import java.util.ArrayList;

import mx.com.washber.washber.Clases.cl_Prenda;

/**
 * Created by ppcoo on 09/01/2018.
 */

public class ws_GuardaServicioRequest {
    public Integer IdCliente;
    public Integer IdProveedor;
    public Integer IdTarjeta;
    public String FPick;
    public String FDrop;
    public String Observaciones;
    public Double Latitud;
    public Double Longitud;
    public String Referencia;
    public ArrayList<cl_Prenda> Prendas;
    public String DeviceId;
    public String TokenCard;
}
