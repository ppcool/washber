package mx.com.washber.washber;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.google.gson.Gson;

import java.util.Calendar;

import mx.com.washber.washber.Clases.cl_Global;
import mx.com.washber.washber.WebService.ws_GenericResult;
import mx.com.washber.washber.WebService.ws_RegisterRequest;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class RegistroActivity extends AppCompatActivity {

    private int mYear, mMonth, mDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
    }

    public void Cancelar(View view) {
        Intent intento = new Intent(this, MainActivity.class);
        startActivity(intento);
        finish();
    }

    public void SelectDate(View view) {
        DialogFragment dialogfragment = new DatePickerDialogTheme();
        dialogfragment.show(getFragmentManager(), "Theme");
    }

    public static class DatePickerDialogTheme extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR) - 18;
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            AlertDialog.Builder builder;

            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, this, year, month, day);

            return datepickerdialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day){

            TextView textview = getActivity().findViewById(R.id.txtFechaNacimiento);

            textview.setText(day + "/" + (month+1) + "/" + year);

        }
    }

    public void Register(View view)
    {
        EditText txtNombre = findViewById(R.id.txtNombre);
        EditText txtFechaNacimiento = findViewById(R.id.txtFechaNacimiento);
        EditText txtCorreoElectronico = findViewById(R.id.txtCorreoElectronico);
        EditText txtLogin = findViewById(R.id.txtLogin);
        EditText txtPassword = findViewById(R.id.txtPassword);
        EditText txtTelefono = findViewById(R.id.txtTelefono);

        String validacion = ValidaCampos();
        if(validacion.length() != 0)
        {
            MyDynamicToast.warningMessage(getApplication(), validacion);
            return;
        }

        clsRegistrar registrar = new clsRegistrar(txtNombre.getText().toString(),txtFechaNacimiento.getText().toString()
                ,txtCorreoElectronico.getText().toString(),txtLogin.getText().toString(), txtPassword.getText().toString()
                ,txtTelefono.getText().toString());

        registrar.execute();

    }

    private String ValidaCampos()
    {
        EditText txtNombre = findViewById(R.id.txtNombre);
        EditText txtFechaNacimiento = findViewById(R.id.txtFechaNacimiento);
        EditText txtCorreoElectronico = findViewById(R.id.txtCorreoElectronico);
        EditText txtLogin = findViewById(R.id.txtLogin);
        EditText txtPassword = findViewById(R.id.txtPassword);
        EditText txtTelefono = findViewById(R.id.txtTelefono);
        String resultado = "";

        if( txtNombre.getText().toString().trim().length() == 0)
        {
            resultado = "Especifica tu nombre";
            txtNombre.requestFocus();
            return resultado;
        }

        if( txtFechaNacimiento.getText().toString().trim().length() == 0)
        {
            resultado = "Especifica tu fecha de nacimiento";
            txtFechaNacimiento.requestFocus();
            return resultado;
        }

        if(txtTelefono.getText().toString().trim().equals(""))
        {
            resultado = "Especifica tu número telefónico";
            txtTelefono.requestFocus();
            return resultado;
        }

        if( txtCorreoElectronico.getText().toString().trim().length() == 0)
        {
            resultado = "Especifica tu correo electrónico";
            txtCorreoElectronico.requestFocus();
            return resultado;
        }

        if( txtLogin.getText().toString().trim().length() == 0)
        {
            resultado = "Especifica tu login";
            txtLogin.requestFocus();
            return resultado;
        }

        if( txtPassword.getText().toString().trim().length() == 0)
        {
            resultado = "Especifica tu password";
            txtPassword.requestFocus();
            return resultado;
        }

        return resultado;
    }

    class clsRegistrar extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private final String pNombre;
        private final String pFNacimiento;
        private final String pCorreo;
        private final String pLogin;
        private final String pPassword;
        private final String pTelefono;
        private String result;

        clsRegistrar(String nombre,String nacimiento, String correo, String login, String password,String telefono)
        {
            pNombre = nombre.trim();
            pFNacimiento = nacimiento.trim();
            pCorreo = correo.trim();
            pLogin = login.trim();
            pPassword = password.trim();
            pTelefono = telefono.trim();
        }


        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(RegistroActivity.this);
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Procesando...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = cl_Global.API_URL + cl_Global.API_REGISTER;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_RegisterRequest requestRegister = new ws_RegisterRequest();
            try {

                requestRegister.Nombre = pNombre;
                requestRegister.FNacimiento = pFNacimiento;
                requestRegister.Correo = pCorreo;
                requestRegister.Login = pLogin;
                requestRegister.Password = pPassword;
                requestRegister.NumeroTelefonico = pTelefono;

                String jsRequest = h.toJson(requestRegister);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();

                return true;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            try
            {
                ws_GenericResult gResult = new Gson().fromJson(String.valueOf(result),ws_GenericResult.class);
                if(success)
                {
                    if(gResult.Codigo == 100)
                    {
                        pDialog.dismiss();
                        RegistroCorrecto();

                    }
                    else if(gResult.Codigo == 102)
                    {
                        pDialog.dismiss();
                        MyDynamicToast.informationMessage(getApplication(), gResult.Mensaje);
                    }
                    else
                    {

                        pDialog.dismiss();
                        MyDynamicToast.errorMessage(getApplication(), gResult.Mensaje);


                    }
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

    private void RegistroCorrecto()
    {
        AlertDialog.Builder alerta = new AlertDialog.Builder(RegistroActivity.this);
        alerta.setMessage("En breve recibirá instrucciones para activar su cuenta. ¡ Bienvenido ! ");
        alerta.setTitle("WashBer");
        alerta.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialogInterface, int i) {
                GotoMain();
            }

        });
        alerta.create().show();


    }

    private void GotoMain()
    {
        Intent intento = new Intent(this,MainActivity.class);
        startActivity(intento);
    }
}