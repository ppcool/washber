package mx.com.washber.washber.Clases;

import me.panavtec.wizard.WizardPage;
import mx.com.washber.washber.ProveedorFragment;

/**
 * Created by ppcoo on 02/01/2018.
 */

public class cl_ProveedorFragment extends WizardPage<ProveedorFragment> {

    @Override
    public ProveedorFragment createFragment()
    {
        return new ProveedorFragment();
    }
}
