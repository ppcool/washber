package mx.com.washber.washber.OpenPay;

/**
 * Created by ppcoo on 16/02/2018.
 */

public interface opOperationCallBack<T> {

    void onError(final opOpenpayServiceException error);

    void onCommunicationError(final opServiceUnavailableException error);

    void onSuccess(final opOperationResult<T> operationResult);
}
