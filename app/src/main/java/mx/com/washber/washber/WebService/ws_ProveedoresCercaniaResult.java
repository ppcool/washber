package mx.com.washber.washber.WebService;

import java.util.ArrayList;

/**
 * Created by ppcoo on 29/12/2017.
 */

public class ws_ProveedoresCercaniaResult {
    public Integer Codigo;
    public String Mensaje;
    public ArrayList<ws_ProveedorCercaniaResult> Proveedores;
}
