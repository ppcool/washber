package mx.com.washber.washber.OpenPay;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import mx.com.washber.washber.Clases.cl_Global;

/**
 * Created by ppcoo on 16/02/2018.
 */

public class opOpenPay {


    // VALORES DE API OPEN PAY
    private opCardService cardService;
    private opTokenService tokenService;
    private opDeviceCollectorDefaultImpl deviceCollectorDefaultImpl;

    public opOpenPay(final String merchantId, final String apiKey, final Boolean productionMode) {

        String baseUrl = cl_Global.URL_OPENPAY;
        opServicesFactory servicesFactory = opServicesFactory.getInstance(baseUrl, merchantId, apiKey);
        this.cardService = servicesFactory.getService(opCardService.class);
        this.tokenService = servicesFactory.getService(opTokenService.class);
        this.deviceCollectorDefaultImpl = new opDeviceCollectorDefaultImpl(baseUrl, merchantId);

    }

    public void createCard(final opCard card, final opOperationCallBack<opCard> operationCallBack) {
        this.createCard(card, null, operationCallBack);
    }

    public void createCard(final opCard card, final String customerId, final opOperationCallBack<opCard> operationCallBack) {
        CardAsyncTask cardAsyncTask = new CardAsyncTask(card, customerId, operationCallBack, this.cardService);
        cardAsyncTask.execute();
    }

    private class CardAsyncTask extends AsyncTask<Void, Void, opOpenPayResult<opCard>> {

        private opCard card;

        private String customerId;

        private opOperationCallBack<opCard> operationCallBack;

        private opCardService cardService;

        public CardAsyncTask(final opCard card, final String customerId, final opOperationCallBack<opCard> operationCallBack, final opCardService cardService) {
            this.card = card;
            this.customerId = customerId;
            this.operationCallBack = operationCallBack;
            this.cardService = cardService;
        }

        @Override
        protected opOpenPayResult<opCard> doInBackground(Void... voids) {
            opOpenPayResult<opCard> openPayResult = new opOpenPayResult<opCard>();
            try {
                opCard newCard = this.cardService.create(this.card, this.customerId);
                openPayResult.setOperationResult(new opOperationResult<opCard>(newCard));
            } catch (opOpenpayServiceException e) {
                openPayResult.setOpenpayServiceException(e);
            } catch (opServiceUnavailableException e) {
                openPayResult.setServiceUnavailableException(e);
            }
            return openPayResult;
        }

        @Override
        protected void onPostExecute(final opOpenPayResult<opCard> result) {
            if (result.getOperationResult() != null) {
                this.operationCallBack.onSuccess(result.getOperationResult());
            } else if (result.getOpenpayServiceException() != null) {
                this.operationCallBack.onError(result.getOpenpayServiceException());
            } else if (result.getServiceUnavailableException() != null) {
                this.operationCallBack.onCommunicationError(result.getServiceUnavailableException());
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    public void createToken(final opCard card, final opOperationCallBack<opToken> operationCallBack) {
        new AsyncTask<Void, Void, opOpenPayResult<opToken>>() {

            @Override
            protected opOpenPayResult<opToken> doInBackground(final Void... params) {
                opOpenPayResult<opToken> openPayResult = new opOpenPayResult<opToken>();
                try {
                    opToken newToken = tokenService.create(card);
                    cl_Global.gblCardToken = newToken.getId().toString();
                    openPayResult.setOperationResult(new opOperationResult<opToken>(newToken));
                } catch (opOpenpayServiceException e) {
                    openPayResult.setOpenpayServiceException(e);
                } catch (opServiceUnavailableException e) {
                    openPayResult.setServiceUnavailableException(e);
                }
                return openPayResult;
            }

            protected void onPostExecute(final opOpenPayResult<opToken> result) {

                if (result.getOperationResult() != null) {
                    operationCallBack.onSuccess(result.getOperationResult());
                } else if (result.getOpenpayServiceException() != null) {
                    operationCallBack.onError(result.getOpenpayServiceException());
                } else if (result.getServiceUnavailableException() != null) {
                    operationCallBack.onCommunicationError(result.getServiceUnavailableException());
                }

            }

        }.execute();
    }

    public opDeviceCollectorDefaultImpl getDeviceCollectorDefaultImpl() {
        return this.deviceCollectorDefaultImpl;
    }
}
