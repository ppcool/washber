package mx.com.washber.washber.WebService;

/**
 * Created by ppcoo on 02/01/2018.
 */

public class ws_SubcategoriaResult {
    public String CadenaPrecio;
    public String Descripcion;
    public Integer IdSubcategoria;
    public String Imagen;
    public String Nombre;
    public float Precio;
}
