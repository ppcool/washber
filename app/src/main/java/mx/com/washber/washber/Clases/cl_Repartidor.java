package mx.com.washber.washber.Clases;

/**
 * Created by ppcoo on 13/01/2018.
 */

public class cl_Repartidor {
    public Integer IdRepartidor;
    public String Nombre;
    public String CorreoElectronico;
    public String Fotografia;
    public String Proveedor;
    public String LogoProveedor;
    public Integer PendienteDrop;
    public Integer PendientePick;
    public Integer DropOff;
    public Integer PickUp;
    public Float MontoMinimoServicio;
}
