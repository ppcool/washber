package mx.com.washber.washber.WebService;

import java.util.ArrayList;

/**
 * Created by ppcoo on 26/12/2017.
 */

public class ws_LoginResult {

    public Integer Codigo;
    public Integer IdCliente;
    public String Mensaje;
    public String Nombre;
    public String Fotografia;
    public Integer DiasServicio;
    public Integer Pendientes;
    public Integer Proceso;
    public Integer Cerrados;
    public Integer Cancelados;
    public float IVA;
    public float MontoMinimoServicio;
    public boolean OpenPayProduction;
}
