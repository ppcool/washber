package mx.com.washber.washber.OpenPay;

import android.app.Activity;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.UUID;

/**
 * Created by ppcoo on 16/02/2018.
 */

public class opDeviceCollectorDefaultImpl {

    private String baseUrl;
    private String merchantId;
    private String errorMessage;

    public opDeviceCollectorDefaultImpl(final String baseUrl, final String merchantId) {
        this.baseUrl = baseUrl;
        this.merchantId = merchantId;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public String setup(final Activity activity) {
        try {
            // Generamos sessionId
            String sessionId = UUID.randomUUID().toString();
            sessionId = sessionId.replace("-", "");

            // Obtenemos identifierForVendor
            String identifierForVendor = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);

            // Inicializamos WebView con el identifierForVendor
            WebView webViewDF = new WebView(activity);
            webViewDF.setWebViewClient(new WebViewClient());
            webViewDF.getSettings().setJavaScriptEnabled(true);
            String identifierForVendorScript = String.format("var identifierForVendor = '%s';", identifierForVendor);
            webViewDF.evaluateJavascript(identifierForVendorScript, null);

            // Ejecutamos OpenControl
            String urlAsString = String.format("%s/oa/logo.htm?m=%s&s=%s", this.baseUrl, this.merchantId, sessionId);
            webViewDF.loadUrl(urlAsString);

            return sessionId;
        } catch (final Exception e) {
            this.logError(this.getClass().getName(), e.getMessage());
            this.errorMessage = e.getMessage();
            return null;
        }
    }

    private void logError(final String tag, final String content) {
        if (content.length() > 4000) {
            Log.e(tag, content.substring(0, 4000));
            this.logError(tag, content.substring(4000));
        } else {
            Log.e(tag, content);
        }
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }
}
