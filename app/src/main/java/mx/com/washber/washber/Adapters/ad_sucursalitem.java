package mx.com.washber.washber.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mx.com.washber.washber.Clases.cl_Sucursal;
import mx.com.washber.washber.R;

/**
 * Created by ppcoo on 02/01/2018.
 */

public class ad_sucursalitem extends ArrayAdapter<cl_Sucursal> {

    public ad_sucursalitem(Context context, ArrayList<cl_Sucursal> elementos)
    {
        super(context,0,elementos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        cl_Sucursal oSucursal = getItem(position);

        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_sucursalitem,parent,false);
        }

        TextView titulo = convertView.findViewById(R.id.lblTitulo);
        TextView direccion = convertView.findViewById(R.id.lblDireccion);
        titulo.setText(oSucursal.Titulo);

        if(oSucursal.Direccion.trim().equals(""))
        {
            direccion.setVisibility(View.GONE);
        }
        else
        {
            direccion.setText(oSucursal.Direccion);
        }
        return convertView;
    }
}
