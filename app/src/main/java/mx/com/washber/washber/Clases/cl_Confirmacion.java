package mx.com.washber.washber.Clases;

import me.panavtec.wizard.WizardPage;
import mx.com.washber.washber.ConfirmacionFragment;

/**
 * Created by ppcoo on 29/12/2017.
 */

public class cl_Confirmacion  extends WizardPage<ConfirmacionFragment> {

    @Override
    public ConfirmacionFragment createFragment()
    {
        return new ConfirmacionFragment();
    }
}
