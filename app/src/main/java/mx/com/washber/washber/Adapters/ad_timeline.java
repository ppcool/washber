package mx.com.washber.washber.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mx.com.washber.washber.Clases.cl_KardexItem;
import mx.com.washber.washber.R;

/**
 * Created by ppcoo on 26/12/2017.
 */

public class ad_timeline extends ArrayAdapter<cl_KardexItem> {

    public ad_timeline(Context context, ArrayList<cl_KardexItem> elementos)
    {
        super(context,0,elementos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        TextView texto;
        cl_KardexItem elemento = getItem(position);

        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_timelineitem,parent,false);
        }

        texto = convertView.findViewById(R.id.lblTitulo);
        texto.setText(elemento.Titulo);

        texto = convertView.findViewById(R.id.lblMensaje);
        texto.setText(elemento.Mensaje);

        texto = convertView.findViewById(R.id.lblFecha);
        texto.setText(elemento.Fecha);
        return convertView;
    }
}
