package mx.com.washber.washber.OpenPay;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by ppcoo on 16/02/2018.
 */

public class opServicesFactory {

    private static volatile opServicesFactory INSTANCE = null;

    private String baseUrl;
    private String merchantId;
    private String apiKey;

    private opServicesFactory(final String baseUrl,
                            final String merchantId, final String apiKey) {
        this.baseUrl = baseUrl;
        this.merchantId = merchantId;
        this.apiKey = apiKey;
    }

    public static opServicesFactory getInstance(final String baseUrl, final String merchantId, final String apiKey) {
        if (INSTANCE == null) {
            synchronized (opServicesFactory.class) {
                if (INSTANCE == null) {
                    INSTANCE = new opServicesFactory(baseUrl, merchantId, apiKey);
                }
            }
        }
        return INSTANCE;
    }

    public <V extends opBaseService<?, ?>> V getService(final Class<V> type) {
        try {
            Constructor<V> ctor = type.getDeclaredConstructor(String.class, String.class, String.class);
            ctor.setAccessible(true);
            return ctor.newInstance(this.baseUrl, this.merchantId, this.apiKey);
            // en todos los casos regresa nulo.
        } catch (NoSuchMethodException e) {
            return null;
        } catch (InstantiationException e) {
            return null;
        } catch (IllegalAccessException e) {
            return null;
        } catch (IllegalArgumentException e) {
            return null;
        } catch (InvocationTargetException e) {
            return null;
        }
    }
}
