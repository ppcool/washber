package mx.com.washber.washber;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.kofigyan.stateprogressbar.StateProgressBar;

import java.util.Calendar;

import me.panavtec.wizard.Wizard;
import me.panavtec.wizard.WizardListener;
import me.panavtec.wizard.WizardPage;
import me.panavtec.wizard.WizardPageListener;
import mx.com.washber.washber.Clases.cl_Confirmacion;
import mx.com.washber.washber.Clases.cl_ConfirmacionServicioFragment;
import mx.com.washber.washber.Clases.cl_PrendasFragment;
import mx.com.washber.washber.Clases.cl_Proveedor;
import mx.com.washber.washber.Clases.cl_ProveedorFragment;
import mx.com.washber.washber.Clases.cl_TarjetaFragment;
import mx.com.washber.washber.Clases.cl_Ubicacion;
import mx.com.washber.washber.OpenPay.opOpenpayServiceException;
import mx.com.washber.washber.OpenPay.opOperationCallBack;
import mx.com.washber.washber.OpenPay.opToken;

public class NuevoServicioActivity extends AppCompatActivity implements WizardPageListener, WizardListener {

    private Wizard wNuevoServicio;
    private String[] descriptionData = {"Ubicación", "Proveedor", "Prendas", "Confirmación"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_servicio);



        /*
        wNuevoServicio = new Wizard.Builder(this, new cl_Ubicacion(), new cl_ProveedorFragment(),
                new cl_PrendasFragment(),new cl_Confirmacion()).containerId(android.R.id.content)
                .pageListener(this)
                .wizardListener(this)
                .build();
        */

        wNuevoServicio = new Wizard.Builder(this, new cl_Ubicacion(), new cl_ProveedorFragment(),
                new cl_PrendasFragment(),new cl_TarjetaFragment(),new cl_ConfirmacionServicioFragment()).containerId(android.R.id.content)
                .pageListener(this)
                .wizardListener(this)
                .build();

        wNuevoServicio.init();

    }

    public Wizard getWizard()
    {
        return wNuevoServicio;
    }

    @Override
    public void onPageChanged(int currentPageIndex, WizardPage page)
    {
        StateProgressBar stateProgressBar = findViewById(R.id.pgrBar);
        //stateProgressBar.setStateDescriptionData(descriptionData);
    }

    @Override
    public void onWizardFinished()
    {

    }
}
