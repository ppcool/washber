package mx.com.washber.washber.OpenPay;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by ppcoo on 16/02/2018.
 */

@SuppressLint("SimpleDateFormat")
public class opCardValidator {

    public static boolean validateCard(final String holderName, final String cardNumber, final Integer expMonth, final Integer expYear, final String cvv){
        return validateHolderName(holderName)
                && validateCVV(cvv, cardNumber)
                && validateExpiryDate(expMonth, expYear)
                && validateNumber(cardNumber);

    }

    public static boolean validateHolderName(final String holderName){
        return holderName != null && holderName.trim().length() > 0;
    }

    public static boolean validateCVV(final String cvv, final String cardNumber) {
        if (cvv == null || cvv.trim().length() == 0) {
            return false;
        }

        opCardType type = getType(cardNumber);
        if (opCardType.AMEX.equals(type) && cvv.trim().length() != 4) {
            return false;
        }


        if ((opCardType.MASTERCARD.equals(type) || opCardType.VISA.equals(type)) && cvv.trim().length() != 3) {
            return false;
        }

        if (opCardType.UNKNOWN.equals(type)){
            return false;
        }

        return true;
    }

    public static boolean validateExpiryDate(final Integer expirationMonth, final Integer expirationYear){
        if (!validateMonth(expirationMonth)){
            return false;
        }

        if (expirationYear == null){
            return false;
        }

        Calendar today = new GregorianCalendar();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-yy");
        Date expirationDate;
        try {
            expirationDate = dateFormat.parse(expirationMonth + "-" + expirationYear);
            Calendar cardExpiration = new GregorianCalendar();
            cardExpiration.setTime(expirationDate);

            if (cardExpiration.get(Calendar.YEAR) > today.get(Calendar.YEAR)) {
                return true;
            } else if (cardExpiration.get(Calendar.YEAR) == today.get(Calendar.YEAR)
                    && cardExpiration.get(Calendar.MONTH) >= today.get(Calendar.MONTH)) {
                return true;
            }

        } catch (ParseException e) {

        }

        return false;

    }

    public static boolean validateMonth(final Integer month) {
        if (month == null) {
            return false;
        }

        return (month >= 1 && month <= 12);

        //validar fecha pasada
    }

    public static boolean validateNumber(final String number){

        if (number == null || number.trim().length() == 0){
            return false;
        }

        if (!opLuhnValidator.passesLuhnTest(number)){
            return false;
        }

        String trimNumber = number.trim();
        opCardType type = getType(number);

        if (opCardType.AMEX.equals(type) && trimNumber.length() != 15) {
            return false;
        } else if (opCardType.MASTERCARD.equals(type) && trimNumber.length() != 16){
            return false;
        } else if (opCardType.VISA.equals(type) && !(trimNumber.length() == 16 || trimNumber.length() == 13)){
            return false;
        }  else if (opCardType.UNKNOWN.equals(type)){
            return false;
        }

        return true;
    }

    public static boolean startsWith(final String cardNumber, final String... prefixes) {
        for (String prefix : prefixes) {
            if (cardNumber.startsWith(prefix)) {
                return true;
            }
        }

        return false;
    }

    public static opCardType getType(final String cardNumber) {
        if (cardNumber != null && cardNumber.trim().length() > 0) {
            if (startsWith(cardNumber, "34", "37")) {
                return opCardType.AMEX;
            } else if (startsWith(cardNumber, "4")) {
                return opCardType.VISA;
            } else if (startsWith(cardNumber, "51", "52", "53", "54", "55")) {
                return opCardType.MASTERCARD;
            }
        }

        return opCardType.UNKNOWN;
    }
}
