package mx.com.washber.washber;


import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.kofigyan.stateprogressbar.StateProgressBar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import mx.com.washber.washber.Clases.cl_Categoria;
import mx.com.washber.washber.Clases.cl_Global;
import mx.com.washber.washber.Clases.cl_NuevoServicio;
import mx.com.washber.washber.Clases.cl_Proveedor;
import mx.com.washber.washber.Clases.cl_Proveedores;
import mx.com.washber.washber.Clases.cl_Subcategoria;
import mx.com.washber.washber.Clases.cl_Sucursal;
import mx.com.washber.washber.WebService.ws_CategoriaCercaniaResult;
import mx.com.washber.washber.WebService.ws_ProveedorCercaniaResult;
import mx.com.washber.washber.WebService.ws_ProveedoresCercaniaRequest;
import mx.com.washber.washber.WebService.ws_ProveedoresCercaniaResult;
import mx.com.washber.washber.WebService.ws_SubcategoriaResult;
import mx.com.washber.washber.WebService.ws_SucursalCercaniaResult;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class UbicacionFragment  extends Fragment implements OnMapReadyCallback {

    private Button goNextButton;
    private Button goBackButton;
    private Button cancelButton;
    private String[] descriptionData = {"Ubicación", "Proveedor", "Prendas", "Confirmación"};
    private MapView mapView;
    private GoogleMap googleMap;
    Location myLocation;
    LocationManager locationManager;
    private EditText txtFechaPick;
    private EditText txtFechaDrop;
    private EditText txtReferencia;

    public UbicacionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ubicacion, container, false);

        //TOOLBAR
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Ubicación");

        //BARRA DE PROGRESO
        StateProgressBar stateProgressBar = view.findViewById(R.id.pgrBar);
        //stateProgressBar.setStateDescriptionData(descriptionData);

        //BOTONES
        goNextButton = view.findViewById(R.id.goNextButton);
        goBackButton = view.findViewById(R.id.goBackButton);
        cancelButton = view.findViewById(R.id.cmdCancelar);
        goBackButton.setVisibility(View.GONE);

        goNextButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                DataValidation();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                Cancel();
            }
        });
        //MAPA
        mapView = view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);


        //Fechas
        txtFechaPick = view.findViewById(R.id.txtPickup);
        txtFechaPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialogfragment = new DatePickerDialogTheme();
                dialogfragment.show(getActivity().getFragmentManager(), "Theme");
            }
        }) ;

        txtFechaDrop = view.findViewById(R.id.txtDrop);
        txtFechaDrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialogfragment = new DatePickerDialogThemeDrop();
                dialogfragment.show(getActivity().getFragmentManager(), "Theme");
            }
        }) ;

        //Referencia
        txtReferencia = view.findViewById(R.id.txtReferencia);
        return  view;
    }

    android.location.LocationListener locationListener = new android.location.LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (googleMap != null) {
                if (location != null)
                {
                    googleMap.clear();
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    myLocation = location;

                    googleMap.addMarker(new MarkerOptions().position(latLng).title("Ubicación actual"));

                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 19);
                    googleMap.animateCamera(cameraUpdate);

                }
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private void prepareLocationUpdates() {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        String provider = locationManager.getBestProvider(criteria, true);

        if (provider != null) {
            if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                            PackageManager.PERMISSION_GRANTED) {
                locationManager.requestLocationUpdates(provider, 1 * 60 * 1000, 10, locationListener);
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        2);
            }
        }
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
    }

    @Override
    public void onMapReady(GoogleMap gMap) {
        googleMap = gMap;
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);

            // Create a criteria object to retrieve provider
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(true);
            criteria.setPowerRequirement(Criteria.POWER_LOW);
            // Get the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);

            // Ubicación actual
            myLocation = locationManager.getLastKnownLocation(provider);

            if (myLocation != null)
            {

                double latitude = myLocation.getLatitude();
                double longitude = myLocation.getLongitude();
                LatLng latLng = new LatLng(latitude, longitude);

                //Marcador de ubicación
                MarkerOptions marcador = new MarkerOptions();
                marcador.position(new LatLng(latitude, longitude));
                marcador.title("Ubicación de Pick-Up");
                marcador.draggable(true);
                googleMap.addMarker(marcador);



                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 2);
                googleMap.animateCamera(cameraUpdate);
            }


        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    1);
        }



    }

    @Override
    public void onPause() {
        super.onPause();
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (googleMap != null)
            googleMap.setMyLocationEnabled(false);
        locationManager.removeUpdates(locationListener);
    }

    public void onResume() {
        super.onResume();

        mapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        mapView.getMapAsync(this);

        prepareLocationUpdates();


        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        String provider = locationManager.getBestProvider(criteria, true);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    2);
        } else
            locationManager.requestLocationUpdates(provider, 1 * 60 * 1000, 10, locationListener);


    }

    public static class DatePickerDialogTheme extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, this, year, month, day);

            return datepickerdialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day){

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            TextView textview = getActivity().findViewById(R.id.txtPickup);
            Integer numeroDia;
            textview.setText(day + "/" + (month+1) + "/" + year);

            //De acuerdo a la fecha de pick asignamos fecha de drop
            Date fecha = new Date();
            try {
                fecha = sdf.parse(textview.getText().toString());
                Calendar c = Calendar.getInstance();
                c.setTime(fecha);
                numeroDia = c.get(Calendar.DAY_OF_WEEK);
                if(numeroDia == 1)
                {
                    MyDynamicToast.warningMessage(getActivity(), "Nuestro servicio de pick-up es de lunes a sábado.");
                    textview.setText("");
                    textview = getActivity().findViewById(R.id.txtDrop);
                    textview.setText("");
                    return;
                }
                c.add(Calendar.DATE,cl_Global.gblUsuario.DiasServicio);
                numeroDia = c.get(Calendar.DAY_OF_WEEK);
                if(numeroDia == 1)
                {
                    c.add(Calendar.DATE,1);
                }
                day = c.get(Calendar.DAY_OF_MONTH);
                month = c.get(Calendar.MONTH);
                year = c.get(Calendar.YEAR);
                textview = getActivity().findViewById(R.id.txtDrop);
                textview.setText(day + "/" + (month+1) + "/" + year);
            }
            catch (Exception e) {
                textview = getActivity().findViewById(R.id.txtDrop);
                textview.setText("");
            }


        }
    }

    public static class DatePickerDialogThemeDrop extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            TextView txtPick = getActivity().findViewById(R.id.txtPickup);
            Date fecha = new Date();
            Date fechaDrop;
            int year = 0;
            int month = 0;
            int day = 0;
            int numeroDia;
            try
            {
                if(!txtPick.getText().toString().equals("")) {
                    fecha = sdf.parse(txtPick.getText().toString());
                    Calendar c = Calendar.getInstance();
                    c.setTime(fecha);
                    c.add(Calendar.DATE,cl_Global.gblUsuario.DiasServicio);
                    fechaDrop = c.getTime();
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                }
                else
                {
                    final Calendar calendar = Calendar.getInstance();
                    year = calendar.get(Calendar.YEAR);
                    month = calendar.get(Calendar.MONTH);
                    day = calendar.get(Calendar.DAY_OF_MONTH) + cl_Global.gblUsuario.DiasServicio + 1;
                }

                DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                        AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, this, year, month, day);

                return datepickerdialog;
            }
            catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public void onDateSet(DatePicker view, int year, int month, int day){
            Date fecha = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Integer numeroDia;
            TextView textview = getActivity().findViewById(R.id.txtDrop);

            textview.setText(day + "/" + (month+1) + "/" + year);

            try
            {
                fecha = sdf.parse(textview.getText().toString());
                Calendar c = Calendar.getInstance();
                c.setTime(fecha);
                numeroDia = c.get(Calendar.DAY_OF_WEEK);
                if(numeroDia == 1)
                {
                    MyDynamicToast.warningMessage(getActivity(), "Nuestro servicio de drop-off es de lunes a sábado.");
                    textview.setText("");
                    return;
                }
            }
            catch(Exception ex)
            {
                textview.setText("");
            }

        }
    }

    private void DataValidation()
    {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaPick;
        Date fechaMinima;
        Date fechaDrop;

        if(txtFechaPick.getText().toString().equals(""))
        {
            MyDynamicToast.warningMessage(getActivity(), "Especifica la fecha de pick-up.");
            return;
        }
        else if(txtFechaDrop.getText().toString().equals(""))
        {
            MyDynamicToast.warningMessage(getActivity(), "Especifica la fecha de drop-off.");
            return;
        }
        else if (txtReferencia.getText().toString().trim().equals(""))
        {
            MyDynamicToast.warningMessage(getActivity(), "Especifica alguna referencia para agilizar nuestro servicio");
            return;
        }
        else if(myLocation == null)
        {
            MyDynamicToast.warningMessage(getActivity(), "No es posible determinar tu ubicación");
            return;
        }

        cl_Global.gblNuevoServicio = new cl_NuevoServicio();
        cl_Global.gblNuevoServicio.Referencia = txtReferencia.getText().toString().trim();
        cl_Global.gblNuevoServicio.Ubicacion = myLocation;
        try
        {
            fechaPick = format.parse(txtFechaPick.getText().toString());
            fechaDrop = format.parse(txtFechaDrop.getText().toString());
            Calendar c = Calendar.getInstance();
            c.setTime(fechaPick);
            c.add(Calendar.DATE,cl_Global.gblUsuario.DiasServicio);
            fechaMinima = c.getTime();
            if(fechaMinima.after(fechaDrop))
            {
                MyDynamicToast.warningMessage(getActivity(), "La fecha de drop-off debe ser al menos " + cl_Global.gblUsuario.DiasServicio.toString() + " después de pick-up");
                return;
            }
            cl_Global.gblNuevoServicio.FechaPickUp = txtFechaPick.getText().toString();

            cl_Global.gblNuevoServicio.FechaDropOff = txtFechaDrop.getText().toString();
        }
        catch (ParseException e)
        {
            MyDynamicToast.errorMessage(getActivity(), "Tuvimos un problema al procesar la información, intenta más tarde.");
            return;
        }

        getProveedores getPro = new getProveedores();
        getPro.execute();
    }

    class getProveedores extends AsyncTask<Void,Void, Boolean>
    {
        private ProgressDialog pDialog;
        private String result;

        getProveedores()
        {
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(getActivity());
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Procesando...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = cl_Global.API_URL + cl_Global.API_PROVEEDORESCERCANIA;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_ProveedoresCercaniaRequest oRequest = new ws_ProveedoresCercaniaRequest();

            try {
                oRequest.Longitud = cl_Global.gblNuevoServicio.Ubicacion.getLongitude();
                oRequest.Latitud = cl_Global.gblNuevoServicio.Ubicacion.getLatitude();
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            cl_Proveedor oProveedor;
            cl_Sucursal oSucursal;
            cl_Categoria oCategoria;
            cl_Subcategoria oSub;
            ws_ProveedorCercaniaResult oP;
            ws_SucursalCercaniaResult oS;
            ws_CategoriaCercaniaResult oC;
            ws_SubcategoriaResult oSubCat;
            try
            {
                ws_ProveedoresCercaniaResult oResult = new Gson().fromJson(String.valueOf(result),ws_ProveedoresCercaniaResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        if(oResult.Proveedores.size() == 0)
                        {
                            MyDynamicToast.warningMessage(getActivity(), "No contamos con proveedores cerca de tu ubicación.");
                            pDialog.dismiss();
                        }
                        else
                        {
                            cl_Global.gblProveedores =new cl_Proveedores();
                            cl_Global.gblProveedores.Proveedores = new ArrayList<cl_Proveedor>();
                            for(Integer i = 0; i < oResult.Proveedores.size(); i++)
                            {
                                oProveedor = new cl_Proveedor();
                                oP = oResult.Proveedores.get(i);
                                oProveedor.IdProveedor = oP.IdProveedor;
                                oProveedor.RazonSocial = oP.RazonSocial;
                                oProveedor.Logotipo = oP.Logotipo;
                                oProveedor.Sucursales = new ArrayList<cl_Sucursal>();
                                oProveedor.Categorias = new ArrayList<cl_Categoria>();

                                for(Integer s= 0; s < oP.Sucursales.size(); s++)
                                {
                                    oS = oP.Sucursales.get(s);
                                    oSucursal = new cl_Sucursal();
                                    oSucursal.IdSucursal = oS.IdSucursal;
                                    oSucursal.Titulo = oS.Titulo;
                                    oSucursal.Direccion = oS.Direccion;
                                    oProveedor.Sucursales.add(oSucursal);
                                }

                                for(Integer c=0; c < oP.Categorias.size(); c++)
                                {
                                    oC = oP.Categorias.get(c);
                                    oCategoria = new cl_Categoria();
                                    oCategoria.IdCategoria = oC.IdCategoria;
                                    oCategoria.Nombre = oC.Nombre;
                                    oCategoria.Subcategorias = new ArrayList<cl_Subcategoria>();
                                    for(Integer sc = 0; sc < oC.Subcategorias.size();sc++)
                                    {
                                        oSubCat = oC.Subcategorias.get(sc);
                                        oSub = new cl_Subcategoria();
                                        oSub.CadenaPrecio = oSubCat.CadenaPrecio;
                                        oSub.Descripcion =oSubCat.Descripcion;
                                        oSub.IdSubcategoria = oSubCat.IdSubcategoria;
                                        oSub.Imagen = oSubCat.Imagen;
                                        oSub.Nombre = oSubCat.Nombre;
                                        oSub.Precio = oSubCat.Precio;

                                        oCategoria.Subcategorias.add(oSub);
                                    }
                                    oProveedor.Categorias.add(oCategoria);
                                }

                                cl_Global.gblProveedores.Proveedores.add(oProveedor);
                            }
                            pDialog.dismiss();
                            NextStep();
                        }
                    }
                    else if(oResult.Codigo == 101)
                    {
                        MyDynamicToast.errorMessage(getActivity(), oResult.Mensaje);
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.warningMessage(getActivity(), oResult.Mensaje);
                        pDialog.dismiss();
                    }
                }

            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public void  NextStep()
    {
        ((NuevoServicioActivity) getActivity()).getWizard().navigateNext();
    }

    public void  Cancel()
    {
        Intent intent = new Intent(getActivity(),PrincipalActivity.class);
        startActivity(intent);
        getActivity().finish();
    }
}
