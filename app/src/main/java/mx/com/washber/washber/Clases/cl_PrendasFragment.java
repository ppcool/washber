package mx.com.washber.washber.Clases;

import me.panavtec.wizard.WizardPage;
import mx.com.washber.washber.PrendasFragment;

/**
 * Created by ppcoo on 02/01/2018.
 */

public class cl_PrendasFragment extends WizardPage<PrendasFragment> {

    @Override
    public PrendasFragment createFragment()
    {
        return new PrendasFragment();
    }
}
