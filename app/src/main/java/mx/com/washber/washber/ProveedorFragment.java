package mx.com.washber.washber;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kofigyan.stateprogressbar.StateProgressBar;
import com.squareup.picasso.Picasso;

import mx.com.washber.washber.Adapters.ad_categoriaitem;
import mx.com.washber.washber.Adapters.ad_proveedorescombo;
import mx.com.washber.washber.Adapters.ad_sucursalitem;
import mx.com.washber.washber.Clases.cl_Global;
import mx.com.washber.washber.Clases.cl_Proveedor;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProveedorFragment extends Fragment {

    private Button goNextButton;
    private Button goBackButton;
    private Button cancelButton;
    private ImageView imglogo;
    private Spinner cmbproveedores;
    private ad_proveedorescombo provAdapeter;
    private cl_Proveedor oProveedor;
    ListView listViewSucursales;
    ListView listViewCategorias;

    public ProveedorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_proveedor, container, false);
        View view = inflater.inflate(R.layout.fragment_proveedor, container, false);

        //TOOLBAR
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Proveedor");

        //BARRA DE PROGRESO
        StateProgressBar stateProgressBar = view.findViewById(R.id.pgrBar);
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);

        //BOTONES
        goNextButton = view.findViewById(R.id.goNextButton);
        goNextButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                cl_Global.gblNuevoServicio.Proveedor = oProveedor;
                ((NuevoServicioActivity) getActivity()).getWizard().navigateNext();
            }
        });

        goBackButton = view.findViewById(R.id.goBackButton);
        goBackButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ((NuevoServicioActivity) getActivity()).getWizard().navigatePrevious();
            }
        });

        cancelButton = view.findViewById(R.id.cmdCancelar);
        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                Cancel();
            }
        });

        //LISTA DE SUCURSALES Y CATEGORIAS
        listViewSucursales = view.findViewById(R.id.lstSucursales);
        listViewCategorias = view.findViewById(R.id.lstCategorias);

        //LOGO DE PROVEEDOR
        imglogo = view.findViewById(R.id.imgLogo);

        //SPINNER
        cmbproveedores = view.findViewById(R.id.cmbProveedores);
        cmbproveedores.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                oProveedor = cl_Global.gblProveedores.Proveedores.get(pos);
                if(cl_Global.gblNuevoServicio.Proveedor != null)
                {
                    if(cl_Global.gblNuevoServicio.Proveedor.IdProveedor != oProveedor.IdProveedor)
                    {
                        cl_Global.gblNuevoServicio.Prendas = null;
                    }
                }
                DisplaySucursalesCategorias(view);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {    }
        });
        provAdapeter = new ad_proveedorescombo(getContext(),cl_Global.gblProveedores.Proveedores);
        cmbproveedores.setAdapter(provAdapeter);


        return  view;

    }

    private void DisplaySucursalesCategorias(View view)
    {
        ad_sucursalitem adapterSuc = new ad_sucursalitem(getContext(),oProveedor.Sucursales);
        ad_categoriaitem adapterCat = new ad_categoriaitem(getContext(),oProveedor.Categorias);

        //Picasso.with(this).load(cl_Global.gblUsuario.Fotografia).into(imgUsuario);
        Picasso.with(getContext()).load(oProveedor.Logotipo).into(imglogo);
        listViewSucursales.setAdapter(adapterSuc);
        listViewCategorias.setAdapter(adapterCat);
    }

    public void  Cancel()
    {
        Intent intent = new Intent(getActivity(),PrincipalActivity.class);
        startActivity(intent);
        getActivity().finish();
    }
}
