package mx.com.washber.washber.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import mx.com.washber.washber.Clases.cl_Subcategoria;
import mx.com.washber.washber.R;

/**
 * Created by ppcoo on 03/01/2018.
 */

public class ad_subcategoriacombo extends BaseAdapter {

    Context context;
    LayoutInflater inflter;
    ArrayList<cl_Subcategoria> oSubcategorias;

    public ad_subcategoriacombo (Context applicationContext,ArrayList<cl_Subcategoria> Subcategorias)
    {
        this.context = applicationContext;
        this.inflter = (LayoutInflater.from(applicationContext));
        this.oSubcategorias = Subcategorias;
    }

    @Override
    public int getCount() {return oSubcategorias.size();}

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {return 0;}

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        view = inflter.inflate(R.layout.layout_subcategoriacomboitem, null);
        cl_Subcategoria oSubcategoria = oSubcategorias.get(i);

        TextView Nombre = view.findViewById(R.id.lblNombre);
        Nombre.setText(oSubcategoria.Nombre);

        return view;
    }
}
