package mx.com.washber.washber.OpenPay;

/**
 * Created by ppcoo on 16/02/2018.
 */

public enum opCardType {
    VISA,
    MASTERCARD,
    AMEX,
    UNKNOWN
}
