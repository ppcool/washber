package mx.com.washber.washber.OpenPay;

import com.google.api.client.util.Key;

/**
 * Created by ppcoo on 16/02/2018.
 */

public class opToken {

    @Key
    private String id;

    @Key
    private opCard card;

    @Override
    public String toString() {
        return String.format("Token [id=%s, card=%s]", this.id, this.card);
    }

    public opToken id(final String id) {
        this.id = id;
        return this;
    }

    public opToken card(final opCard card) {
        this.card = card;
        return this;
    }

    public String getId() {
        return this.id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public opCard getCard() {
        return this.card;
    }

}
