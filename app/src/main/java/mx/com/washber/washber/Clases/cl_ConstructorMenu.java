package mx.com.washber.washber.Clases;

import android.graphics.Color;

import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.HamButton;

import mx.com.washber.washber.R;

/**
 * Created by ppcoo on 16/01/2018.
 */

public class cl_ConstructorMenu {

    private static int[] arrayImagenes = new int[]
            {
                    R.drawable.ic_nuevoservicio,
                    R.drawable.ic_enproceso,
                    R.drawable.ic_historial,
                    R.drawable.ic_tarjeta,
                    R.drawable.ic_cerrarsesion
            };

    private static int imageResourceIndex = 0;

    static int getImageResource() {
        if (imageResourceIndex >= arrayImagenes.length) imageResourceIndex = 0;
        return arrayImagenes[imageResourceIndex++];
    }

    public static HamButton.Builder getHamButtonBuilder(String text, String subText, int BackColor,int TextColor) {

        return new HamButton.Builder()
                .normalImageRes(getImageResource())
                .normalText(text)
                .subNormalText(subText)
                .pieceColor(TextColor)
                .normalTextColor(TextColor)
                .normalColor(BackColor);
    }


    private static cl_ConstructorMenu ourInstance = new cl_ConstructorMenu();

    public static cl_ConstructorMenu getInstance() {
        return ourInstance;
    }

    private cl_ConstructorMenu() {
    }
}
