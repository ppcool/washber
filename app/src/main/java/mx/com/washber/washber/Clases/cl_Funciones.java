package mx.com.washber.washber.Clases;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by ppcoo on 24/01/2018.
 */

public class cl_Funciones {

    Context mContext;

    public cl_Funciones(Context context)
    {
        this.mContext = context;
    }

    public ArrayList<cl_GenericItem> ListaMeses()
    {
        ArrayList<cl_GenericItem> Items;
        cl_GenericItem Item;

        Items = new ArrayList<cl_GenericItem>();

        for(Integer i = 1;i < 13; i++)
        {
            Item = new cl_GenericItem();
            if(i == 1)
            {
                Item.Cadena = "ENERO";
            }
            else if(i == 2)
            {
                Item.Cadena = "FEBRERO";
            }
            else if(i == 3)
            {
                Item.Cadena = "MARZO";
            }
            else if(i == 4)
            {
                Item.Cadena = "ABRIL";
            }
            else if(i == 5)
            {
                Item.Cadena = "MAYO";
            }
            else if(i == 6)
            {
                Item.Cadena = "JUNIO";
            }
            else if(i == 7)
            {
                Item.Cadena = "JULIO";
            }
            else if(i == 8)
            {
                Item.Cadena = "AGOSTO";
            }
            else if(i == 9)
            {
                Item.Cadena = "SEPTIEMBRE";
            }
            else if(i == 10)
            {
                Item.Cadena = "OCTUBRE";
            }
            else if(i == 11)
            {
                Item.Cadena = "NOVIEMBRE";
            }
            else
            {
                Item.Cadena = "DICIEMBRE";
            }
            Item.Valor = i;
            Items.add(Item);
        }

        return Items;
    }
}
