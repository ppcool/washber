package mx.com.washber.washber.OpenPay;

import android.annotation.TargetApi;
import android.os.Build;

import java.io.IOException;

/**
 * Created by ppcoo on 16/02/2018.
 */

public class opServiceUnavailableException extends IOException {

    private static final long serialVersionUID = -7388627000694002585L;

    public opServiceUnavailableException(final String message) {
        super(message);
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public opServiceUnavailableException(final Throwable cause) {
        super(cause);
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public opServiceUnavailableException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
