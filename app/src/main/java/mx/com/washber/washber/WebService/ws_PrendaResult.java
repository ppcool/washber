package mx.com.washber.washber.WebService;

/**
 * Created by ppcoo on 19/01/2018.
 */

public class ws_PrendaResult {
    public String Categoria;
    public String Fotografia;
    public Integer IdPrendaOrden;
    public Integer IdSubcategoria;
    public Integer Piezas;
    public float Precio;
    public String Subcategoria;
}
