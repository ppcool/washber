package mx.com.washber.washber.Clases;

import java.util.ArrayList;

import me.panavtec.wizard.WizardPage;
import mx.com.washber.washber.ProveedorFragment;

/**
 * Created by ppcoo on 29/12/2017.
 */

public class cl_Proveedor  {

    public Integer IdProveedor;
    public String RazonSocial;
    public String Logotipo;
    public ArrayList<cl_Sucursal> Sucursales;
    public ArrayList<cl_Categoria> Categorias;
}
