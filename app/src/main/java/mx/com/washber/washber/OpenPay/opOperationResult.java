package mx.com.washber.washber.OpenPay;

/**
 * Created by ppcoo on 16/02/2018.
 */

public class opOperationResult<T> {

    private T result;

    public opOperationResult(final T result) {
        this.result = result;
    }

    public T getResult() {
        return this.result;
    }

}
