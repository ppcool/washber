package mx.com.washber.washber.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mx.com.washber.washber.Clases.cl_TarjetaCliente;
import mx.com.washber.washber.R;

/**
 * Created by ppcoo on 24/01/2018.
 */

public class ad_tarjetascliente extends ArrayAdapter<cl_TarjetaCliente> {

    public ad_tarjetascliente(Context context, ArrayList<cl_TarjetaCliente> elementos)
    {
        super(context,0,elementos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        cl_TarjetaCliente elemento = getItem(position);

        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_tarjetaitem,parent,false);
        }

        TextView etiqueta = convertView.findViewById(R.id.lblTipo);
        etiqueta.setText(elemento.TipoNombre);

        etiqueta = convertView.findViewById(R.id.lblNumero);
        etiqueta.setText(elemento.NumeroTarjeta);

        etiqueta = convertView.findViewById(R.id.lblVencimiento);
        etiqueta.setText("Vencimiento: " + elemento.Vencimiento);

        return convertView;
    }
}
