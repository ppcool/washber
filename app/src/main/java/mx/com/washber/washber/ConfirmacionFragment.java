package mx.com.washber.washber;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.google.gson.Gson;
import com.kofigyan.stateprogressbar.StateProgressBar;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;

import mx.com.washber.washber.Adapters.ad_tarjetacombo;
import mx.com.washber.washber.Clases.cl_Global;
import mx.com.washber.washber.Clases.cl_Prenda;
import mx.com.washber.washber.Clases.cl_Tarjeta;
import mx.com.washber.washber.WebService.ws_GenericResult;
import mx.com.washber.washber.WebService.ws_GuardaServicioRequest;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmacionFragment extends Fragment {

    private Button goNextButton;
    private Button goBackButton;
    private Button cancelButton;
    private Button confirmButton;
    private Spinner cmbtarjetas;
    private TextView lblcvv;

    private ad_tarjetacombo adTarjetas;
    public ConfirmacionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_confirmacion, container, false);

        //TOOLBAR
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Confirmación");

        //BARRA DE PROGRESO
        StateProgressBar stateProgressBar = view.findViewById(R.id.pgrBar);
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);

        //BOTONES
        goNextButton = view.findViewById(R.id.goNextButton);
        goBackButton = view.findViewById(R.id.goBackButton);

        goNextButton.setVisibility(View.GONE);
        goBackButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ((NuevoServicioActivity) getActivity()).getWizard().navigatePrevious();
            }
        });

        cancelButton = view.findViewById(R.id.cmdCancelar);
        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                Cancel();
            }
        });

        confirmButton = view.findViewById(R.id.cmdConfirmar);
        confirmButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                DataValidation();
            }
        });

        //TARJETAS
        cmbtarjetas = view.findViewById(R.id.cmbTarjetas);
        cmbtarjetas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                cl_Global.gblNuevoServicio.Tarjeta = cl_Global.gblUsuario.Tarjetas.get(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){    }
        });

        adTarjetas = new ad_tarjetacombo(getContext(),cl_Global.gblUsuario.Tarjetas);
        cmbtarjetas.setAdapter(adTarjetas);

        //CVV
        lblcvv = view.findViewById(R.id.txtCSV);

        ServiceData(view);

        return  view;
    }

    public void  Cancel()
    {
        Intent intent = new Intent(getActivity(),PrincipalActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    public void ServiceData(View view)
    {
        DateFormat df = new android.text.format.DateFormat();
        cl_Prenda prenda;
        Integer valor;
        float subTotal;
        float iva;

        //PICK UP
        TextView lblEtiqueta = view.findViewById(R.id.lblFechaPickup);
        lblEtiqueta.setText("Pick-Up "  + cl_Global.gblNuevoServicio.FechaPickUp);

        //DROP OFF
        lblEtiqueta = view.findViewById(R.id.lblFechaDropOff);
        lblEtiqueta.setText("Drop-Off "  + cl_Global.gblNuevoServicio.FechaDropOff);

        //PRENDAS
        valor = cl_Global.gblNuevoServicio.Prendas.size();
        lblEtiqueta = view.findViewById(R.id.lblPrendas);
        lblEtiqueta.setText(valor.toString());

        //PIEZAS
        lblEtiqueta = view.findViewById(R.id.lblPiezas);
        subTotal = 0;
        for(Integer i =0 ; i< cl_Global.gblNuevoServicio.Prendas.size(); i++)
        {
            prenda = cl_Global.gblNuevoServicio.Prendas.get(i);
            subTotal += prenda.Precio * prenda.Piezas;
        }

        lblEtiqueta.setText(valor.toString());

        //PROVEEDOR
        lblEtiqueta = view.findViewById(R.id.lblProveedor);
        lblEtiqueta.setText(cl_Global.gblNuevoServicio.Proveedor.RazonSocial);

        //SUBTOTAL
        NumberFormat format = NumberFormat.getCurrencyInstance();
        lblEtiqueta = view.findViewById(R.id.lblSubTotal);
        lblEtiqueta.setText(format.format(subTotal));

        //IVA
        lblEtiqueta = view.findViewById(R.id.lblIVA);
        iva = subTotal * cl_Global.gblUsuario.IVA / 100;
        lblEtiqueta.setText(format.format(iva));

        //TOTAL
        lblEtiqueta = view.findViewById(R.id.lblTotal);
        lblEtiqueta.setText(format.format(iva + subTotal));
    }

    private void DataValidation()
    {

        if(lblcvv.getText().toString().equals(""))
        {
            MyDynamicToast.warningMessage(getContext(), "Especifica el valor CVV de tu tarjeta");
            return;
        }

        saveService oSaveService = new saveService();
        oSaveService.execute();
    }

    class saveService extends AsyncTask<Void,Void, Boolean>
    {
        private ProgressDialog pDialog;
        private String result;

        saveService(){}

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(getActivity());
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Procesando...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = cl_Global.API_URL + cl_Global.API_GUARDASERVICIO;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_GuardaServicioRequest oRequest = new ws_GuardaServicioRequest();

            try {
                oRequest.IdCliente = cl_Global.gblUsuario.IdCliente;
                oRequest.IdProveedor = cl_Global.gblNuevoServicio.Proveedor.IdProveedor;
                oRequest.IdTarjeta = cl_Global.gblNuevoServicio.Tarjeta.IdTarjeta;
                oRequest.FPick = cl_Global.gblNuevoServicio.FechaPickUp;
                oRequest.FDrop = cl_Global.gblNuevoServicio.FechaDropOff;
                oRequest.Observaciones = "";
                oRequest.Longitud = cl_Global.gblNuevoServicio.Ubicacion.getLongitude();
                oRequest.Latitud = cl_Global.gblNuevoServicio.Ubicacion.getLatitude();
                oRequest.Referencia = cl_Global.gblNuevoServicio.Referencia;
                oRequest.Prendas = cl_Global.gblNuevoServicio.Prendas;

                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            try
            {
                ws_GenericResult oResult = new Gson().fromJson(String.valueOf(result),ws_GenericResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        MyDynamicToast.successMessage(getActivity(), oResult.Mensaje);
                        pDialog.dismiss();
                        closeWizard();
                    }
                    else if (oResult.Codigo == 102)
                    {
                        MyDynamicToast.informationMessage(getActivity(), oResult.Mensaje);
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.errorMessage(getActivity(), "Tuvimos un problema al registrar tu servicio, intenta nuevamente.");
                        pDialog.dismiss();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

    private void closeWizard()
    {
        Intent i = new Intent(getContext(),PrincipalActivity.class);
        startActivity(i);
        getActivity().finish();
    }
}
