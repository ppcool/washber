package mx.com.washber.washber.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mx.com.washber.washber.Clases.cl_Categoria;
import mx.com.washber.washber.R;

/**
 * Created by ppcoo on 03/01/2018.
 */

public class ad_categoriacombo extends BaseAdapter {

    Context context;
    LayoutInflater inflter;
    ArrayList<cl_Categoria> oCategorias;

    public ad_categoriacombo (Context applicationContext,ArrayList<cl_Categoria> Categorias)
    {
        this.context = applicationContext;
        inflter = (LayoutInflater.from(applicationContext));
        oCategorias = Categorias;
    }

    @Override
    public int getCount() {return oCategorias.size();}

    @Override
    public Object getItem(int i) {return null;}

    @Override
    public long getItemId(int i) {return 0;}

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        view = inflter.inflate(R.layout.layout_genericcomboitem, null);
        cl_Categoria oCategoria = oCategorias.get(i);
        TextView Texto = view.findViewById(R.id.lblTexto);
        Texto.setText(oCategoria.Nombre);

        return view;
    }
}
