package mx.com.washber.washber.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mx.com.washber.washber.Clases.cl_Prenda;
import mx.com.washber.washber.R;

/**
 * Created by ppcoo on 03/01/2018.
 */

public class ad_prendaitem extends ArrayAdapter<cl_Prenda> {

    public ad_prendaitem (Context context, ArrayList<cl_Prenda> elementos)
    {
        super(context,0,elementos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        cl_Prenda oPrenda = getItem(position);

        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_prendaitem,parent,false);
        }

        TextView Categoria = convertView.findViewById(R.id.lblCategoria);
        Categoria.setText(oPrenda.Categoria);

        TextView Subcategoria = convertView.findViewById(R.id.lblSubcategoria);
        Subcategoria.setText(oPrenda.Subcategoria);

        TextView Piezas = convertView.findViewById(R.id.lblPiezas);
        Piezas.setText("Piezas " + oPrenda.Piezas.toString());

        TextView Precio = convertView.findViewById(R.id.lblPrecio);
        Precio.setText("Precio por pieza " + oPrenda.CadenaPrecio);

        return convertView;
    }
}
