package mx.com.washber.washber.Clases;

/**
 * Created by ppcoo on 26/12/2017.
 */

public class cl_DetalleServicio {
    public Integer PrendasOrden;
    public Integer PrendasP;
    public String Proveedor;
    public String FechaCreacion;
    public String FechaSolicitadaP;
    public String FechaProgramadaP;
    public String FechaRealP;
    public  String FechaSolicitadaD;
    public String FechaProgramadaD;
    public String FechaRealD;
    public String Observaciones;
    public Float Latitud;
    public Float Longitud;
    public String Referencia;
    public String Estatus;
    public String LogoProveedor;
}
