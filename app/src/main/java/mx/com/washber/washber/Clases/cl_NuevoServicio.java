package mx.com.washber.washber.Clases;

import android.location.Location;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ppcoo on 02/01/2018.
 */

public class cl_NuevoServicio {
    public String FechaPickUp;
    public String FechaDropOff;
    public Location Ubicacion;
    public String Referencia;
    public ArrayList<cl_Prenda> Prendas;
    public cl_Proveedor Proveedor;
    public cl_Tarjeta Tarjeta;
}
