package mx.com.washber.washber.Clases;

import java.util.ArrayList;

/**
 * Created by ppcoo on 26/12/2017.
 */

public class cl_Usuario {
    public Integer IdCliente;
    public String Nombre;
    public String Fotografia;
    public ArrayList<cl_Tarjeta> Tarjetas;
    public Integer DiasServicio;
    public Integer Pendientes;
    public Integer Proceso;
    public Integer Cerrados;
    public Integer Cancelados;
    public float IVA;
    public float MontoMinimoServicio;
}
