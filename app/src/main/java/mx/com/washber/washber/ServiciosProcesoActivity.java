package mx.com.washber.washber;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import mx.com.washber.washber.Adapters.ad_servicioheader;
import mx.com.washber.washber.Clases.cl_DetalleServicio;
import mx.com.washber.washber.Clases.cl_Global;
import mx.com.washber.washber.Clases.cl_ServicioH;
import mx.com.washber.washber.WebService.ws_DetalleServicioResult;
import mx.com.washber.washber.WebService.ws_ServicioRequest;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class ServiciosProcesoActivity extends AppCompatActivity {

    ListView listView;
    ad_servicioheader adapter;
    cl_DetalleServicio servicio;
    cl_ServicioH oServicio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicios_proceso);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Servicios en proceso");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = findViewById(R.id.lstServicios);
        adapter = new ad_servicioheader(this, cl_Global.gblServiciosHeader.Servicios);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                oServicio = (cl_ServicioH) listView.getItemAtPosition(i);
                GetDetail gDetail = new GetDetail(oServicio.IdOrden);
                gDetail.execute();
            }
        });

        listView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }

    public void DisplayDetail()
    {
        final Dialog dialog  = new Dialog(this);

        dialog.setContentView(R.layout.layout_modalservicio);

        Button dismissButton = dialog.findViewById(R.id.cmdCerrar);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //ETIQUETAS DEL MODAL
        TextView texto;
        ImageView imgProveedor;

        texto = dialog.findViewById(R.id.txtFolioModalServicio);
        texto.setText("Folio: " + oServicio.Folio);

        texto = dialog.findViewById(R.id.txtEstatusServicio);
        texto.setText(servicio.Estatus);

        texto = dialog.findViewById(R.id.txtCreacionServicio);
        texto.setText("Registrado: " + servicio.FechaCreacion);

        texto = dialog.findViewById(R.id.txtSolicitudPModalServicio);
        texto.setText(servicio.FechaSolicitadaP);

        texto = dialog.findViewById(R.id.txtSolicitudDModalServicio);
        texto.setText(servicio.FechaSolicitadaD);

        if(servicio.FechaProgramadaP.equals(""))
        {
            texto = dialog.findViewById(R.id.txtProgramadoPModalServicio);
            texto.setText("PENDIENTE");
            texto = dialog.findViewById(R.id.txtProgramadoDModalServicio);
            texto.setText("PENDIENTE");
        }
        else
        {
            texto = dialog.findViewById(R.id.txtProgramadoPModalServicio);
            texto.setText(servicio.FechaProgramadaP);
            texto = dialog.findViewById(R.id.txtProgramadoDModalServicio);
            if(servicio.FechaProgramadaD.equals(""))
            {
                texto.setText("PENDIENTE");
            }
            else
            {
                texto.setText(servicio.FechaProgramadaD);
            }
        }

        if(servicio.FechaRealP.equals(""))
        {
            texto = dialog.findViewById(R.id.txtRealPModalServicio);
            texto.setText("PENDIENTE");
            texto = dialog.findViewById(R.id.txtRealDModalServicio);
            texto.setText("PENDIENTE");
            texto = dialog.findViewById(R.id.txtPrendasPModalServicio);
            texto.setText("");
        }
        else
        {
            texto = dialog.findViewById(R.id.txtRealPModalServicio);
            texto.setText(servicio.FechaRealP);
            texto = dialog.findViewById(R.id.txtRealDModalServicio);
            if(servicio.FechaRealD.equals(""))
            {
                texto.setText("PENDIENTE");
            }
            else
            {
                texto.setText(servicio.FechaRealD);
            }
            texto = dialog.findViewById(R.id.txtPrendasPModalServicio);
            texto.setVisibility(TextView.VISIBLE);
            if(servicio.PrendasP == 1)
            {
                texto.setText("Una prenda en pick-up");
            }
            else
            {
                texto.setText(servicio.PrendasP.toString() + " prendas en pick-up");
            }
        }

        texto = dialog.findViewById(R.id.txtPrendasModalServicio);
        if(servicio.PrendasOrden == 1)
        {
            texto.setText("Una prenda en el servicio");
        }
        else
        {
            texto.setText(servicio.PrendasOrden.toString() + " prendas en el servicio");
        }

        texto = dialog.findViewById(R.id.txtProveedorModalServicio);
        texto.setText("Proveedor: " + servicio.Proveedor);

        imgProveedor = dialog.findViewById(R.id.imgLogoProveedor);
        if(servicio.LogoProveedor.equals(""))
        {
            imgProveedor.setImageResource(R.drawable.iconosmall);
        }
        else
        {
            Picasso.with(this).load(servicio.LogoProveedor).into(imgProveedor);
        }
        dialog.setTitle("Detalle");
        dialog.setCancelable(false);
        dialog.show();
    }

    class GetDetail extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private final Integer idOrden;
        private String result;

        GetDetail(Integer IdOrden)
        {
            idOrden = IdOrden;
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(ServiciosProcesoActivity.this);
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Obteniendo servicio...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = cl_Global.API_URL + cl_Global.API_SERVICIODETALLE;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_ServicioRequest oRequest = new ws_ServicioRequest();

            try
            {
                oRequest.IdOrden = idOrden;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();

                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;

            }
        }

        protected void onPostExecute(final Boolean success)
        {
            try
            {
                ws_DetalleServicioResult oResult = new Gson().fromJson(String.valueOf(result),ws_DetalleServicioResult.class);

                if(oResult.Codigo == 101)
                {
                    MyDynamicToast.errorMessage(getApplication(), "Tuvimos un problema al obtener la información, intenta mas tarde.");
                }
                else if(oResult.Codigo == 100)
                {
                    servicio = new cl_DetalleServicio();
                    servicio.PrendasOrden = oResult.PrendasOrden;
                    servicio.PrendasP = oResult.PrendasP;
                    servicio.Proveedor = oResult.Proveedor;
                    servicio.FechaCreacion = oResult.FechaCreacion;
                    servicio.FechaSolicitadaP = oResult.FechaSolicitadaP;
                    servicio.FechaProgramadaP = oResult.FechaProgramadaP;
                    servicio.FechaRealP = oResult.FechaRealP;
                    servicio.FechaSolicitadaD = oResult.FechaSolicitadaD;
                    servicio.FechaProgramadaD = oResult.FechaProgramadaD;
                    servicio.FechaRealD = oResult.FechaRealD;
                    servicio.Observaciones = oResult.Observaciones;
                    servicio.Latitud = oResult.Latitud;
                    servicio.Longitud = oResult.Longitud;
                    servicio.Referencia = oResult.Referencia;
                    servicio.Estatus = oResult.Estatus;
                    servicio.LogoProveedor = oResult.LogoProveedor;
                    DisplayDetail();
                }
                else
                {
                    MyDynamicToast.warningMessage(getApplication(), oResult.Mensaje);
                }

                pDialog.dismiss();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }
}
