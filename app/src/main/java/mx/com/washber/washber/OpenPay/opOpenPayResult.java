package mx.com.washber.washber.OpenPay;

/**
 * Created by ppcoo on 16/02/2018.
 */

public class opOpenPayResult<T> {

    private opOpenpayServiceException openpayServiceException;

    private opOperationResult<T> operationResult;

    private opServiceUnavailableException serviceUnavailableException;

    public opOpenpayServiceException getOpenpayServiceException() {
        return this.openpayServiceException;
    }

    public void setOpenpayServiceException(final opOpenpayServiceException openpayServiceException) {
        this.openpayServiceException = openpayServiceException;
    }

    public opOperationResult<T> getOperationResult() {
        return this.operationResult;
    }

    public void setOperationResult(final opOperationResult<T> operationResult) {
        this.operationResult = operationResult;
    }

    public opServiceUnavailableException getServiceUnavailableException() {
        return this.serviceUnavailableException;
    }

    public void setServiceUnavailableException(final opServiceUnavailableException serviceUnavailableException) {
        this.serviceUnavailableException = serviceUnavailableException;
    }

}
