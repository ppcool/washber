package mx.com.washber.washber.OpenPay;

/**
 * Created by ppcoo on 16/02/2018.
 */

public class opTokenService  extends opBaseService<opCard, opToken> {

    private static final String MERCHANT_TOKENS = "tokens";

    public opTokenService(final String baseUrl, final String merchantId, final String apiKey) {
        super(baseUrl, merchantId, apiKey, opToken.class);
    }

    public opToken create(final opCard card) throws opOpenpayServiceException, opServiceUnavailableException {
        return this.post(MERCHANT_TOKENS, card);
    }
}
