package mx.com.washber.washber;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.google.gson.Gson;

import java.util.ArrayList;

import mx.com.washber.washber.Adapters.ad_servicioheader;
import mx.com.washber.washber.Adapters.ad_timeline;
import mx.com.washber.washber.Clases.cl_DetalleServicio;
import mx.com.washber.washber.Clases.cl_Global;
import mx.com.washber.washber.Clases.cl_Kardex;
import mx.com.washber.washber.Clases.cl_KardexItem;
import mx.com.washber.washber.Clases.cl_ServicioH;
import mx.com.washber.washber.WebService.ws_DetalleServicioResult;
import mx.com.washber.washber.WebService.ws_KardexItemResult;
import mx.com.washber.washber.WebService.ws_KardexResult;
import mx.com.washber.washber.WebService.ws_ServicioRequest;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HistorialActivity extends AppCompatActivity {

    ListView listView;
    ad_servicioheader adapter;
    cl_DetalleServicio servicio;
    cl_ServicioH oServicio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Historial de servicios");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = findViewById(R.id.lstServicios);
        adapter = new ad_servicioheader(this, cl_Global.gblServiciosHeader.Servicios);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                oServicio = (cl_ServicioH) listView.getItemAtPosition(i);
                GetKardex getKardex = new GetKardex();
                getKardex.execute();
            }
        });

        listView.setAdapter(adapter);
    }

    public void DisplayTimeLine()
    {
        ListView listView;
        ad_timeline adapter;

        final Dialog dialog  = new Dialog(this);

        dialog.setContentView(R.layout.layout_modaltimeline);

        Button dismissButton = dialog.findViewById(R.id.cmdCerrar);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        listView = dialog.findViewById(R.id.lstTimeLine);
        adapter = new ad_timeline(this,cl_Global.gblKardex.Kardex);

        listView.setAdapter(adapter);
        dialog.setTitle("Historial del servicio");
        dialog.setCancelable(false);
        dialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
    class GetKardex extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private String result;
        private final Integer idOrden;

        GetKardex()
        {
            idOrden = oServicio.IdOrden;
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(HistorialActivity.this);
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Obteniendo kardex...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = cl_Global.API_URL + cl_Global.API_KARDEX;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_ServicioRequest oRequest = new ws_ServicioRequest();
            try
            {
                oRequest.IdOrden = idOrden;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();

                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;

            }
        }

        protected void onPostExecute(final Boolean success)
        {
            cl_KardexItem oItem;
            try
            {
                ws_KardexResult oResult = new Gson().fromJson(String.valueOf(result),ws_KardexResult.class);

                if(oResult.Codigo == 101)
                {
                    MyDynamicToast.errorMessage(getApplication(), "Tuvimos un problema al obtener la información, intenta mas tarde.");
                }
                else if(oResult.Codigo == 100)
                {
                    cl_Global.gblKardex = new cl_Kardex();
                    cl_Global.gblKardex.Kardex = new ArrayList<cl_KardexItem>();
                    for(int i = 0; i < oResult.Kardex.size(); i++)
                    {
                        ws_KardexItemResult r = oResult.Kardex.get(i);
                        oItem = new cl_KardexItem(
                            r.Titulo,
                            r.Mensaje,
                            r.Fecha
                        );

                        cl_Global.gblKardex.Kardex.add(oItem);
                    }

                    pDialog.dismiss();
                    DisplayTimeLine();
                }
                else
                {
                    MyDynamicToast.informationMessage(getApplication(), oResult.Mensaje);
                }

                pDialog.dismiss();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }


}
