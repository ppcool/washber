package mx.com.washber.washber.Clases;

import me.panavtec.wizard.WizardPage;
import mx.com.washber.washber.ConfirmacionServicioFragment;
import mx.com.washber.washber.PrendasFragment;

/**
 * Created by ppcoo on 19/02/2018.
 */

public class cl_ConfirmacionServicioFragment extends WizardPage<ConfirmacionServicioFragment> {

    @Override
    public ConfirmacionServicioFragment createFragment()
    {
        return new ConfirmacionServicioFragment();
    }
}
