package mx.com.washber.washber.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import mx.com.washber.washber.Clases.cl_Proveedor;
import mx.com.washber.washber.R;

/**
 * Created by ppcoo on 02/01/2018.
 */

public class ad_proveedorescombo extends BaseAdapter {

    Context context;
    ArrayList<cl_Proveedor> proveedores;
    LayoutInflater inflter;

    public ad_proveedorescombo(Context applicationContext, ArrayList<cl_Proveedor> Proveedores)
    {
        this.context = applicationContext;
        inflter = (LayoutInflater.from(applicationContext));
        this.proveedores = Proveedores;
    }

    @Override
    public int getCount() {return proveedores.size();}

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {return 0;}

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view = inflter.inflate(R.layout.layout_proveedorcomboitem, null);
        TextView razonSocial = view.findViewById(R.id.lblRazonSocial);

        cl_Proveedor prov = proveedores.get(i);
        razonSocial.setText(prov.RazonSocial);

        return view;
    }
}
