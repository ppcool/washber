package mx.com.washber.washber.Clases;

import java.util.ArrayList;

/**
 * Created by ppcoo on 02/01/2018.
 */

public class cl_Categoria {
    public Integer IdCategoria;
    public String Nombre;
    public String Descripción;
    public ArrayList<cl_Subcategoria> Subcategorias;
}
