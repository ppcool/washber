package mx.com.washber.washber.WebService;

import java.util.ArrayList;

/**
 * Created by ppcoo on 29/12/2017.
 */

public class ws_CategoriaCercaniaResult {
    public Integer IdCategoria;
    public String Nombre;
    public ArrayList<ws_SubcategoriaResult> Subcategorias;
}
