package mx.com.washber.washber.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mx.com.washber.washber.Clases.cl_Categoria;
import mx.com.washber.washber.R;

/**
 * Created by ppcoo on 02/01/2018.
 */

public class ad_categoriaitem extends ArrayAdapter<cl_Categoria> {

    public ad_categoriaitem(Context context, ArrayList<cl_Categoria> elementos)
    {
        super(context,0,elementos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        cl_Categoria oCategoria = getItem(position);

        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_categoriaitem,parent,false);
        }

        TextView nombre = convertView.findViewById(R.id.lblNombre);
        nombre.setText(oCategoria.Nombre);

        return convertView;
    }
}
