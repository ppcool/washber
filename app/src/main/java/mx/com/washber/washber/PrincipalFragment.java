package mx.com.washber.washber;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.google.gson.Gson;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;
import com.rilixtech.materialfancybutton.MaterialFancyButton;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import mx.com.washber.washber.Adapters.ad_genericcombo;
import mx.com.washber.washber.Clases.cl_ConstructorMenu;
import mx.com.washber.washber.Clases.cl_Funciones;
import mx.com.washber.washber.Clases.cl_GenericItem;
import mx.com.washber.washber.Clases.cl_Global;
import mx.com.washber.washber.Clases.cl_ServicioH;
import mx.com.washber.washber.Clases.cl_ServiciosH;
import mx.com.washber.washber.Clases.cl_Tarjeta;
import mx.com.washber.washber.Clases.cl_TarjetaCliente;
import mx.com.washber.washber.Clases.cl_TarjetasCliente;
import mx.com.washber.washber.WebService.ws_GenericIntRequest;
import mx.com.washber.washber.WebService.ws_GenericResult;
import mx.com.washber.washber.WebService.ws_IdUsuarioRequest;
import mx.com.washber.washber.WebService.ws_ServicioHResult;
import mx.com.washber.washber.WebService.ws_ServiciosHResult;
import mx.com.washber.washber.WebService.ws_TarjetaClienteResult;
import mx.com.washber.washber.WebService.ws_TarjetaRequest;
import mx.com.washber.washber.WebService.ws_TarjetasClienteResult;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class PrincipalFragment extends Fragment {

    private Button cmdNuevo;
    private BoomMenuButton bmb;
    String a;
    int keyDel;
    ArrayList<cl_GenericItem> Anios;
    ArrayList<cl_GenericItem> Meses;
    ArrayList<cl_GenericItem> Tipos;
    cl_GenericItem AnioSeleccion;
    cl_GenericItem MesSeleccion;
    cl_GenericItem TipoSeleccion;

    public PrincipalFragment() {
        // Required empty public constructor
    }

    public static PrincipalFragment newInstance()
    {
        PrincipalFragment fragment = new PrincipalFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        MaterialFancyButton boton;
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_principal, container, false);

        TextView lblEtiqueta = v.findViewById(R.id.lblNombreUsuario);
        lblEtiqueta.setText(cl_Global.gblUsuario.Nombre);

        /*
        ETIQUETAS


        lblEtiqueta = v.findViewById(R.id.lblPendientes);
        lblEtiqueta.setText("PENDIENTES DE PICK-UP " + cl_Global.gblUsuario.Pendientes.toString());

        lblEtiqueta = v.findViewById(R.id.lblProceso);
        lblEtiqueta.setText("SERVICIOS EN PROCESO " + cl_Global.gblUsuario.Proceso.toString());

        lblEtiqueta = v.findViewById(R.id.lblCerrados);
        lblEtiqueta.setText("SERVICIOS CERRADOS " + cl_Global.gblUsuario.Cerrados.toString());

        lblEtiqueta = v.findViewById(R.id.lblCancelados);
        lblEtiqueta.setText("SERVICIOS CANCELADOS " + cl_Global.gblUsuario.Cancelados.toString());
        */

        //BOTON DE NUEVO SERVICIO
        cmdNuevo = v.findViewById(R.id.cmdNuevoServicio);

        cmdNuevo.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                NewService();
            }
        });

        /*
        //MENU INFERIOR
        bmb = v.findViewById(R.id.bmb);
        bmb.setButtonEnum(ButtonEnum.Ham);

        bmb.setPiecePlaceEnum(PiecePlaceEnum.HAM_5);
        bmb.setButtonPlaceEnum(ButtonPlaceEnum.HAM_5);

        bmb.addBuilder(cl_ConstructorMenu.getHamButtonBuilder("Nuevo servicio","Registra un nuevo servicio."
                ,getResources().getColor(R.color.colorBlue),getResources().getColor(R.color.colorWhite)).listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {NewService();}
        }));

        bmb.addBuilder(cl_ConstructorMenu.getHamButtonBuilder("Servicios en proceso","Tus servicios en proceso"
                ,getResources().getColor(R.color.colorBlue),getResources().getColor(R.color.colorWhite)).listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
            getProcessServiciosHeader obtenServicios = new getProcessServiciosHeader(cl_Global.gblUsuario.IdCliente);
            obtenServicios.execute();
            }
        }));

        bmb.addBuilder(cl_ConstructorMenu.getHamButtonBuilder("Historial de servicios","Tu historial",
                getResources().getColor(R.color.colorBlue),getResources().getColor(R.color.colorWhite)).listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                getServices getS = new getServices();
                getS.execute();
            }
        }));

        bmb.addBuilder(cl_ConstructorMenu.getHamButtonBuilder("Tarjetas","Tus tarjetas registradas",
                getResources().getColor(R.color.colorBlue),getResources().getColor(R.color.colorWhite)).listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                getTarjetas getT = new getTarjetas(cl_Global.gblUsuario.IdCliente);
                getT.execute();
            }
        }));

        bmb.addBuilder(cl_ConstructorMenu.getHamButtonBuilder("Cerrar sesión",""
                ,getResources().getColor(R.color.colorBlue),getResources().getColor(R.color.colorWhite)).listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                CloseSession();
            }
        }));

        bmb.setUse3DTransformAnimation(true);
        */

        //------------------------------------------------------------ BOTONES
        // Nuevo servicio
        boton = v.findViewById(R.id.cmdNuevo);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                NewService();
            }
        });

        // Servicios en proceso
        boton = v.findViewById(R.id.cmdProceso);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                getProcessServiciosHeader obtenServicios = new getProcessServiciosHeader(cl_Global.gblUsuario.IdCliente);
                obtenServicios.execute();
            }
        });

        // Historial
        boton = v.findViewById(R.id.cmdHistorial);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                getServices getS = new getServices();
                getS.execute();
            }
        });

        // Tarjetas
        /*
        boton = v.findViewById(R.id.cmdTarjetas);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                getTarjetas getT = new getTarjetas(cl_Global.gblUsuario.IdCliente);
                getT.execute();
            }
        });
        */

        // Cerrar sesión
        boton = v.findViewById(R.id.cmdCerrar);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                CloseSession();
            }
        });

        return v;
    }

    public void CloseSession()
    {
        cl_Global.gblUsuario = null;
        Intent i = new Intent(getContext(),MainActivity.class);
        startActivity(i);
        getActivity().finish();
    }

    public void NewService ()
    {
        final PrettyDialog mdlMonto;
        NumberFormat format = NumberFormat.getCurrencyInstance();
        /*
        if(cl_Global.gblUsuario.Tarjetas.size() == 0)
        {
            //MyDynamicToast.warningMessage(getContext(), "Es necesario que registres tu tarjeta bancaria para solicitar servicios.");
            DisplayNewTarjeta();
        }
        else
        {
            if(cl_Global.gblUsuario.MontoMinimoServicio > 0) {
                //ENVIAMOS MODAL DE MONTO MÍNIMO
                mdlMonto = new PrettyDialog(getContext());
                mdlMonto.setTitle("WashBer");
                mdlMonto.setAnimationEnabled(true);
                mdlMonto.setMessage("Te recordamos que el monto mínimo de servicio es de " + format.format(cl_Global.gblUsuario.MontoMinimoServicio) + ".");
                mdlMonto.setIcon(R.drawable.ic_info);

                mdlMonto.addButton(
                        "Aceptar",     // button text
                        R.color.pdlg_color_white,  // button text color
                        R.color.colorBlue,  // button background color
                        new PrettyDialogCallback() {  // button OnClick listener
                            @Override
                            public void onClick() {
                                Intent i = new Intent(getContext(), NuevoServicioActivity.class);
                                startActivity(i);
                            }
                        }
                );

                mdlMonto.addButton(
                        "Cancelar",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_red,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                mdlMonto.dismiss();
                            }
                        }
                );

                mdlMonto.show();
            }
            else
            {
                Intent i = new Intent(getContext(), NuevoServicioActivity.class);
                startActivity(i);
            }

        }
        */

        if(cl_Global.gblUsuario.MontoMinimoServicio > 0) {
            //ENVIAMOS MODAL DE MONTO MÍNIMO
            mdlMonto = new PrettyDialog(getContext());
            mdlMonto.setTitle("WashBer");
            mdlMonto.setAnimationEnabled(true);
            mdlMonto.setMessage("Te recordamos que el monto mínimo de servicio es de " + format.format(cl_Global.gblUsuario.MontoMinimoServicio) + ".");
            mdlMonto.setIcon(R.drawable.ic_info);

            mdlMonto.addButton(
                    "Aceptar",     // button text
                    R.color.pdlg_color_white,  // button text color
                    R.color.colorBlue,  // button background color
                    new PrettyDialogCallback() {  // button OnClick listener
                        @Override
                        public void onClick() {
                            Intent i = new Intent(getContext(), NuevoServicioActivity.class);
                            startActivity(i);
                        }
                    }
            );

            mdlMonto.addButton(
                    "Cancelar",
                    R.color.pdlg_color_white,
                    R.color.pdlg_color_red,
                    new PrettyDialogCallback() {
                        @Override
                        public void onClick() {
                            mdlMonto.dismiss();
                        }
                    }
            );

            mdlMonto.show();
        }
        else
        {
            Intent i = new Intent(getContext(), NuevoServicioActivity.class);
            startActivity(i);
        }
    }

    public void DisplayServices()
    {
        Intent i = new Intent(getContext(),ServiciosProcesoActivity.class);
        startActivity(i);
    }

    public void DisplayHistory()
    {
        Intent i = new Intent(getContext(),HistorialActivity.class);
        startActivity(i);
    }

    class getProcessServiciosHeader extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private final Integer pIdUsuario;
        private String result;

        getProcessServiciosHeader(Integer IdUsuario)
        {
            pIdUsuario = IdUsuario;
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(getContext());
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Obteniendo servicios...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = cl_Global.API_URL + cl_Global.API_SERVICIOSPROCESS;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_IdUsuarioRequest oRequest = new ws_IdUsuarioRequest();

            try
            {
                oRequest.IdCliente = cl_Global.gblUsuario.IdCliente;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;

            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;

            }
        }

        protected void onPostExecute(final Boolean success)
        {
            cl_ServicioH ser;
            try
            {
                ws_ServiciosHResult oResult  = new Gson().fromJson(String.valueOf(result),ws_ServiciosHResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100) {
                        if(oResult.Servicios.size() == 0)
                        {
                            pDialog.dismiss();
                            MyDynamicToast.warningMessage(getContext(), "No tienes servicios en proceso.");
                        }
                        else
                        {

                            cl_Global.gblServiciosHeader = new cl_ServiciosH();
                            cl_Global.gblServiciosHeader.Servicios = new ArrayList<cl_ServicioH>();
                            //cl_Global.gblServiciosHeader.Servicios = oResult.Servicios;
                            for(int i= 0; i < oResult.Servicios.size(); i++)
                            {
                                ws_ServicioHResult r = oResult.Servicios.get(i);
                                ser = new cl_ServicioH();
                                ser.Estatus = r.Estatus;
                                ser.Fecha = r.Fecha;
                                ser.Folio = r.Folio;
                                ser.IdOrden = r.IdOrden;
                                cl_Global.gblServiciosHeader.Servicios.add(ser);
                            }
                            pDialog.dismiss();
                            DisplayServices();
                        }
                    }
                    else if(oResult.Codigo == 101)
                    {
                        MyDynamicToast.errorMessage(getContext(), "Tenemos un problema con la información, reintenta mas tarde.");
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.warningMessage(getContext(), oResult.Mensaje);
                        pDialog.dismiss();
                    }
                }
                else
                {
                    pDialog.dismiss();
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                pDialog.dismiss();
            }
        }
    }

    class getServices extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private final Integer pIdUsuario;
        private String result;

        getServices()
        {
            pIdUsuario = cl_Global.gblUsuario.IdCliente;
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(getContext());
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Obteniendo servicios...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = cl_Global.API_URL + cl_Global.API_SERVICIOS;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_IdUsuarioRequest oRequest = new ws_IdUsuarioRequest();

            try
            {
                oRequest.IdCliente = cl_Global.gblUsuario.IdCliente;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;

            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;

            }
        }

        protected void onPostExecute(final Boolean success)
        {
            cl_ServicioH ser;
            try
            {
                ws_ServiciosHResult oResult  = new Gson().fromJson(String.valueOf(result),ws_ServiciosHResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100) {
                        if(oResult.Servicios.size() == 0)
                        {
                            pDialog.dismiss();
                            MyDynamicToast.warningMessage(getContext(), "No has registrado servicios.");
                        }
                        else
                        {

                            cl_Global.gblServiciosHeader = new cl_ServiciosH();
                            cl_Global.gblServiciosHeader.Servicios = new ArrayList<cl_ServicioH>();
                            for(int i= 0; i < oResult.Servicios.size(); i++)
                            {
                                ws_ServicioHResult r = oResult.Servicios.get(i);
                                ser = new cl_ServicioH();
                                ser.Estatus = r.Estatus;
                                ser.Fecha = r.Fecha;
                                ser.Folio = r.Folio;
                                ser.IdOrden = r.IdOrden;
                                cl_Global.gblServiciosHeader.Servicios.add(ser);
                            }
                            pDialog.dismiss();
                            DisplayHistory();
                        }
                    }
                    else if(oResult.Codigo == 101)
                    {
                        MyDynamicToast.errorMessage(getContext(), "Tenemos un problema con la información, reintenta mas tarde.");
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.warningMessage(getContext(), oResult.Mensaje);
                        pDialog.dismiss();
                    }
                }
                else
                {
                    pDialog.dismiss();
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                pDialog.dismiss();
            }
        }
    }

    class getTarjetas extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private final Integer pIdUsuario;
        private String result;

        getTarjetas (Integer IdUsuario) {
            this.pIdUsuario = IdUsuario;
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(getContext());
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Obteniendo tarjetas...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = cl_Global.API_URL + cl_Global.API_TARJETASC;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_GenericIntRequest oRequest = new ws_GenericIntRequest();

            try
            {
                oRequest.Valor = this.pIdUsuario;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            cl_TarjetaCliente oTarjeta;
            try
            {
                ws_TarjetasClienteResult oResult = new Gson().fromJson(String.valueOf(result),ws_TarjetasClienteResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        cl_Global.gblTarjetas = new cl_TarjetasCliente();
                        cl_Global.gblTarjetas.Tarjetas = new ArrayList<cl_TarjetaCliente>();
                        for(int i= 0; i < oResult.Tarjetas.size(); i++)
                        {
                            ws_TarjetaClienteResult t = oResult.Tarjetas.get(i);

                            oTarjeta = new cl_TarjetaCliente();
                            oTarjeta.IdTarjetaCliente = t.IdTarjetaCliente;
                            oTarjeta.NumeroTarjeta = t.NumeroTarjeta;
                            oTarjeta.Tipo = t.Tipo;
                            oTarjeta.Vencimiento = t.Vencimiento;
                            if(t.Tipo == 1)
                            {
                                oTarjeta.TipoNombre = "VISA";
                            }
                            else if(t.Tipo == 2)
                            {
                                oTarjeta.TipoNombre = "MASTERCARD";
                            }
                            else if(t.Tipo == 3)
                            {
                                oTarjeta.TipoNombre = "AMERICAN EXPRESS";
                            }
                            else
                            {
                                oTarjeta.TipoNombre = "NO IDENTIFICADA";
                            }

                            cl_Global.gblTarjetas.Tarjetas.add(oTarjeta);
                        }

                        pDialog.dismiss();
                        DisplayTarjetas();
                    }
                    else if(oResult.Codigo == 101)
                    {
                        MyDynamicToast.errorMessage(getContext(), "Tenemos un problema con la información, reintenta mas tarde.");
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.warningMessage(getContext(), oResult.Mensaje);
                        pDialog.dismiss();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                pDialog.dismiss();
            }
        }
    }

    public void DisplayTarjetas()
    {
        Intent i = new Intent(getContext(),TarjetasClienteActivity.class);
        startActivity(i);
    }

    public void DisplayNewTarjeta()
    {

        ad_genericcombo adGeneric;
        ArrayList<cl_GenericItem> Items;
        cl_GenericItem Item;
        Integer anio;
        cl_Funciones funciones;

        final Dialog dialog  = new Dialog(getContext());
        dialog.setContentView(R.layout.layout_modaladdtarjeta);

        Button cmdCancelar = dialog.findViewById(R.id.cmdCancelar);
        Button cmdAceptar = dialog.findViewById(R.id.cmdAceptar);

        cmdCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        cmdAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ValidateData(dialog);
            }
        });

        Spinner combo = dialog.findViewById(R.id.cmbTipos);
        //LLENAMOS LISTA DE TIPOS
        Items = new ArrayList<cl_GenericItem>();

        Item = new cl_GenericItem();
        Item.Valor = 1;
        Item.Cadena = "VISA";
        Items.add(Item);

        Item = new cl_GenericItem();
        Item.Valor = 2;
        Item.Cadena = "MASTERCARD";
        Items.add(Item);

        Item = new cl_GenericItem();
        Item.Valor = 3;
        Item.Cadena = "AMERICAN EXPRESS";
        Items.add(Item);

        adGeneric = new ad_genericcombo(getContext(),Items);
        combo.setAdapter(adGeneric);
        Tipos = new ArrayList<cl_GenericItem>();
        Tipos = Items;
        combo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                TipoSeleccion = Tipos.get(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){    }

        });

        //LLENAMOS AÑOS
        Items = new ArrayList<cl_GenericItem>();
        anio = Calendar.getInstance().get(Calendar.YEAR) - 1;

        for(Integer i = 0;i < 6; i++)
        {
            anio = anio + 1;
            Item = new cl_GenericItem();
            Item.Valor = anio;
            Item.Cadena = anio.toString();
            Items.add(Item);
        }
        combo = dialog.findViewById(R.id.cmbAnio);
        adGeneric = new ad_genericcombo(getContext(),Items);
        combo.setAdapter(adGeneric);
        Anios = new ArrayList<cl_GenericItem>();
        Anios = Items;
        combo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                AnioSeleccion = Anios.get(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){    }

        });


        //LLENAMOS MESES
        funciones = new cl_Funciones(getContext());
        Items = new ArrayList<cl_GenericItem>();
        Items = funciones.ListaMeses();

        combo = dialog.findViewById(R.id.cmbMes);
        adGeneric = new ad_genericcombo(getContext(),Items);
        combo.setAdapter(adGeneric);
        Meses = new ArrayList<cl_GenericItem>();
        Meses = Items;
        combo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                MesSeleccion = Meses.get(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){    }

        });

        //EVENTOS PARA TEXTO DE NÜMERO DE TARJETA
        final EditText text = dialog.findViewById(R.id.txtNumero);
        text.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                boolean flag = true;
                String eachBlock[] = text.getText().toString().split("-");

                for (int i = 0; i < eachBlock.length; i++) {
                    if (eachBlock[i].length() > 4) {
                        flag = false;
                    }
                }

                if (flag) {

                    text.setOnKeyListener(new View.OnKeyListener() {

                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {

                            if (keyCode == KeyEvent.KEYCODE_DEL)
                                keyDel = 1;
                            return false;
                        }
                    });

                    if (keyDel == 0) {

                        if (((text.getText().length() + 1) % 5) == 0) {

                            if (text.getText().toString().split("-").length <= 3) {
                                text.setText(text.getText() + "-");
                                text.setSelection(text.getText().length());
                            }
                        }
                        a = text.getText().toString();
                    } else {
                        a = text.getText().toString();
                        keyDel = 0;
                    }

                } else {
                    text.setText(a);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        });

        dialog.setTitle("Registra tu tarjeta");
        dialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void ValidateData(Dialog dialog)
    {
        EditText texto = dialog.findViewById(R.id.txtNumero);
        Integer anio = Calendar.getInstance().get(Calendar.YEAR);
        Integer mes = Calendar.getInstance().get(Calendar.MONTH) + 1;
        ws_TarjetaRequest oRequest = new ws_TarjetaRequest();

        if(texto.getText().toString().equals(""))
        {
            MyDynamicToast.warningMessage(getContext(), "Especifica el número de tu tarjeta");
            return;
        }

        if(AnioSeleccion.Valor <= anio && MesSeleccion.Valor <= mes)
        {
            MyDynamicToast.warningMessage(getContext(), "El vencimiento debe ser mayor al año y mes actual");
            return;
        }

        oRequest.IdCliente = cl_Global.gblUsuario.IdCliente;
        oRequest.Tipo = TipoSeleccion.Valor;
        oRequest.AnioVencimiento = AnioSeleccion.Valor;
        oRequest.MesVencimiento = MesSeleccion.Valor;
        oRequest.NumeroTarjeta = texto.getText().toString().replace("-","");

        saveTarjeta sT = new saveTarjeta(oRequest);
        dialog.dismiss();
        sT.execute();
    }

    class saveTarjeta extends AsyncTask<Void,Void, Boolean>
    {
        private ProgressDialog pDialog;
        private String result;
        private ws_TarjetaRequest oRequest = new ws_TarjetaRequest();

        saveTarjeta(ws_TarjetaRequest Request)
        {
            this.oRequest = Request;
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(getContext());
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Procesando...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = cl_Global.API_URL + cl_Global.API_SAVETARJETAC;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            try
            {
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            try
            {
                ws_GenericResult oResult = new Gson().fromJson(String.valueOf(result),ws_GenericResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        RefreshTarjetas(pDialog);
                    }
                    else if (oResult.Codigo == 102)
                    {
                        MyDynamicToast.informationMessage(getContext(), oResult.Mensaje);
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.errorMessage(getContext(), "Tuvimos un problema al registrar tu tarjeta, intenta nuevamente.");
                        pDialog.dismiss();
                    }
                }
            }
            catch (Exception ex)
            {
                MyDynamicToast.errorMessage(getContext(), "Tuvimos un problema al registrar la tarjeta, intenta nuevamente.");
            }
        }
    }

    public void RefreshTarjetas(ProgressDialog PDialog)
    {
        getRefreshTarjetas dRT = new getRefreshTarjetas(cl_Global.gblUsuario.IdCliente,PDialog);
        dRT.execute();
    }

    class getRefreshTarjetas extends AsyncTask<Void,Void,Boolean>
    {
        private final Integer pIdUsuario;
        private String result;
        ProgressDialog oDialogo;

        getRefreshTarjetas (Integer IdUsuario,ProgressDialog Dialogo) {
            this.pIdUsuario = IdUsuario;
            this.oDialogo = Dialogo;
        }

        @Override
        protected void onPreExecute()
        {
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = cl_Global.API_URL + cl_Global.API_TARJETASC;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_GenericIntRequest oRequest = new ws_GenericIntRequest();

            try
            {
                oRequest.Valor = this.pIdUsuario;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            cl_TarjetaCliente oTarjeta;
            cl_Tarjeta oTarjetaUsuario;
            try
            {
                ws_TarjetasClienteResult oResult = new Gson().fromJson(String.valueOf(result),ws_TarjetasClienteResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        cl_Global.gblTarjetas = new cl_TarjetasCliente();
                        cl_Global.gblTarjetas.Tarjetas = new ArrayList<cl_TarjetaCliente>();
                        cl_Global.gblUsuario.Tarjetas = new ArrayList<cl_Tarjeta>();

                        for(int i= 0; i < oResult.Tarjetas.size(); i++)
                        {
                            ws_TarjetaClienteResult t = oResult.Tarjetas.get(i);

                            oTarjetaUsuario = new cl_Tarjeta();
                            oTarjeta = new cl_TarjetaCliente();
                            oTarjeta.IdTarjetaCliente = t.IdTarjetaCliente;
                            oTarjetaUsuario.IdTarjeta = t.IdTarjetaCliente;
                            oTarjeta.NumeroTarjeta = t.NumeroTarjeta;
                            oTarjetaUsuario.Numero = t.NumeroTarjeta;

                            oTarjeta.Tipo = t.Tipo;
                            oTarjeta.Vencimiento = t.Vencimiento;
                            if(t.Tipo == 1)
                            {
                                oTarjeta.TipoNombre = "VISA";
                            }
                            else if(t.Tipo == 2)
                            {
                                oTarjeta.TipoNombre = "MASTERCARD";
                            }
                            else if(t.Tipo == 3)
                            {
                                oTarjeta.TipoNombre = "AMERICAN EXPRESS";
                            }
                            else
                            {
                                oTarjeta.TipoNombre = "NO IDENTIFICADA";
                            }

                            cl_Global.gblTarjetas.Tarjetas.add(oTarjeta);
                            cl_Global.gblUsuario.Tarjetas.add(oTarjetaUsuario);
                        }
                        oDialogo.dismiss();
                        if(cl_Global.gblUsuario.Tarjetas.size() != 0) {
                            DisplayWizard();
                        }
                    }
                    else if(oResult.Codigo == 101)
                    {
                        oDialogo.dismiss();
                        MyDynamicToast.errorMessage(getContext(), "Tenemos un problema con la información, reintenta mas tarde.");
                    }
                    else
                    {
                        oDialogo.dismiss();
                        MyDynamicToast.warningMessage(getContext(), oResult.Mensaje);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public void DisplayWizard()
    {
        final PrettyDialog mdlMonto;
        NumberFormat format = NumberFormat.getCurrencyInstance();

        if(cl_Global.gblUsuario.MontoMinimoServicio > 0) {
            //ENVIAMOS MODAL DE MONTO MÍNIMO
            mdlMonto = new PrettyDialog(getContext());
            mdlMonto.setTitle("WashBer");
            mdlMonto.setAnimationEnabled(true);
            mdlMonto.setMessage("Te recordamos que el monto mínimo de servicio es de " + format.format(cl_Global.gblUsuario.MontoMinimoServicio) + ".");
            mdlMonto.setIcon(R.drawable.ic_info);

            mdlMonto.addButton(
                    "Aceptar",     // button text
                    R.color.pdlg_color_white,  // button text color
                    R.color.colorBlue,  // button background color
                    new PrettyDialogCallback() {  // button OnClick listener
                        @Override
                        public void onClick() {
                            Intent i = new Intent(getContext(), NuevoServicioActivity.class);
                            startActivity(i);
                        }
                    }
            );

            mdlMonto.addButton(
                    "Cancelar",
                    R.color.pdlg_color_white,
                    R.color.pdlg_color_red,
                    new PrettyDialogCallback() {
                        @Override
                        public void onClick() {
                            mdlMonto.dismiss();
                        }
                    }
            );

            mdlMonto.show();
        }
        else
        {
            Intent i = new Intent(getContext(), NuevoServicioActivity.class);
            startActivity(i);
        }
    }
}
