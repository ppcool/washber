package mx.com.washber.washber;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.kofigyan.stateprogressbar.StateProgressBar;

import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;

import mx.com.washber.washber.Adapters.ad_genericcombo;
import mx.com.washber.washber.Clases.cl_Funciones;
import mx.com.washber.washber.Clases.cl_GenericItem;
import mx.com.washber.washber.Clases.cl_Global;
import mx.com.washber.washber.Clases.cl_Prenda;
import mx.com.washber.washber.OpenPay.opCard;
import mx.com.washber.washber.OpenPay.opCardValidator;
import mx.com.washber.washber.OpenPay.opOpenPay;
import mx.com.washber.washber.OpenPay.opOpenpayServiceException;
import mx.com.washber.washber.OpenPay.opOperationCallBack;
import mx.com.washber.washber.OpenPay.opOperationResult;
import mx.com.washber.washber.OpenPay.opServiceUnavailableException;
import mx.com.washber.washber.OpenPay.opToken;


/**
 * A simple {@link Fragment} subclass.
 */
public class TarjetaFragment extends Fragment implements  opOperationCallBack<opToken> {


    cl_GenericItem AnioSeleccion;
    cl_GenericItem MesSeleccion;
    ArrayList<cl_GenericItem> Anios;
    ArrayList<cl_GenericItem> Meses;
    int keyDel;
    String a;
    ProgressDialog pDialog;

    public TarjetaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Button goNextButton;
        Button goBackButton;
        Button cancelButton;
        TextView etiqueta;
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_tarjeta, container, false);

        //TOOLBAR
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Tarjeta");

        //BARRA DE PROGRESO
        StateProgressBar stateProgressBar = view.findViewById(R.id.pgrBar);
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);

        //BOTONES
        goNextButton = view.findViewById(R.id.goNextButton);
        goNextButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override public void onClick(View v) {
                DataValidation(view);
            }
        });

        goBackButton = view.findViewById(R.id.goBackButton);
        goBackButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ((NuevoServicioActivity) getActivity()).getWizard().navigatePrevious();
            }
        });

        cancelButton = view.findViewById(R.id.cmdCancelar);
        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                Cancel();
            }
        });

        ServiceData(view);

        return view;
    }

    public void  Cancel()
    {
        Intent intent = new Intent(getActivity(),PrincipalActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void DataValidation(View view)
    {
        EditText texto;
        Integer anio = Calendar.getInstance().get(Calendar.YEAR);
        Integer mes = Calendar.getInstance().get(Calendar.MONTH) + 1;
        opCard oCard;
        opOpenPay openPay;
        String deviceID;

        texto = view.findViewById(R.id.txtTitular);
        if(texto.getText().toString().trim().equals(""))
        {
            MyDynamicToast.warningMessage(getActivity(), "Especifica el nombre del titular");
            return;
        }

        texto = view.findViewById(R.id.txtNumero);
        if(texto.getText().toString().trim().equals(""))
        {
            MyDynamicToast.warningMessage(getActivity(), "Especifica el número de tarjeta");
            return;
        }

        if((AnioSeleccion.Valor + 2000) <= anio && (MesSeleccion.Valor) <= mes)
        {
            MyDynamicToast.warningMessage(getActivity(), "El vencimiento debe ser mayor al año y mes actual");
            return;
        }

        texto = view.findViewById(R.id.txtCSV);
        if(texto.getText().toString().trim().equals(""))
        {
            MyDynamicToast.warningMessage(getActivity(), "Especifica CVV de la tarjeta");
            return;
        }

        //VALIDACIONES OPENPAY

        //número de tarjeta
        texto = view.findViewById(R.id.txtNumero);
        if(!opCardValidator.validateNumber(texto.getText().toString().replace("-","")))
        {
            MyDynamicToast.warningMessage(getActivity(), "El número de tarjeta no es válido");
            return;
        }
        oCard = new opCard();
        oCard.cardNumber(texto.getText().toString().replace("-",""));
        cl_Global.gblStrTarjeta = texto.getText().toString();
        //cvv2
        texto = view.findViewById(R.id.txtCSV);
        if(!opCardValidator.validateCVV(texto.getText().toString(),oCard.getCardNumber()))
        {
            MyDynamicToast.warningMessage(getActivity(), "El número de tarjeta no es válido");
            return;
        }
        oCard.cvv2(texto.getText().toString());

        if (!opCardValidator.validateExpiryDate(MesSeleccion.Valor, AnioSeleccion.Valor + 2000))
        {
            MyDynamicToast.warningMessage(getActivity(), "Datos de vencimiento no válidos");
            return;
        }
        oCard.expirationMonth(MesSeleccion.Valor);
        oCard.expirationYear(AnioSeleccion.Valor);

        texto = view.findViewById(R.id.txtTitular);
        oCard.holderName(texto.getText().toString());

        //Obtenemos Device ID
        pDialog = new ProgressDialog(getContext());
        pDialog.setTitle("WashBer");
        pDialog.setMessage("Procesando...");
        pDialog.setCancelable(false);
        pDialog.show();

        //Inicializamos openpay
        if(!cl_Global.OpenPayProduccion) {
            openPay = new opOpenPay(cl_Global.OpenPayID,cl_Global.OpenPayPublica,false);
        }
        else
        {
            openPay = new opOpenPay(cl_Global.OpenPayID,cl_Global.OpenPayPrivada,true);
        }

        deviceID = openPay.getDeviceCollectorDefaultImpl().setup(getActivity());
        if(deviceID == null)
        {
            pDialog.dismiss();
            MyDynamicToast.errorMessage(getActivity(), "Tuvimos un problema con tu tarjeta, intenta nuevamente.");
            return;
        }

        cl_Global.gblDeviceID = deviceID;

        //Obtentemos token de la tarjeta
        cl_Global.gblTarjeta = oCard;
        openPay.createToken(oCard,this);
    }

    public void ServiceData(View view)
    {
        ArrayList<cl_GenericItem> Items;
        cl_GenericItem Item;
        Integer anio;
        cl_Funciones funciones;
        Spinner combo;
        ad_genericcombo adGeneric;
        EditText texto;
        String sAnio;

        //Nombre del cliente
        texto = view.findViewById(R.id.txtTitular);
        texto.setText(cl_Global.gblUsuario.Nombre);

        //LLENAMOS AÑOS
        Items = new ArrayList<cl_GenericItem>();
        anio = Calendar.getInstance().get(Calendar.YEAR) - 1;

        for(Integer i = 0;i < 6; i++)
        {
            anio = anio + 1;
            sAnio =anio.toString();
            sAnio = sAnio.substring(2,4);
            Item = new cl_GenericItem();
            Item.Valor = Integer.parseInt(sAnio);
            Item.Cadena = anio.toString();
            Items.add(Item);
        }
        combo = view.findViewById(R.id.cmbAnio);
        adGeneric = new ad_genericcombo(getContext(),Items);
        combo.setAdapter(adGeneric);
        Anios = new ArrayList<cl_GenericItem>();
        Anios = Items;
        combo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                AnioSeleccion = Anios.get(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){    }

        });

        //LLENAMOS MESES
        funciones = new cl_Funciones(getContext());
        Items = new ArrayList<cl_GenericItem>();
        Items = funciones.ListaMeses();

        combo = view.findViewById(R.id.cmbMes);
        adGeneric = new ad_genericcombo(getContext(),Items);
        combo.setAdapter(adGeneric);
        Meses = new ArrayList<cl_GenericItem>();
        Meses = Items;
        combo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                MesSeleccion = Meses.get(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){    }

        });

        //EVENTOS PARA TEXTO DE NÜMERO DE TARJETA
        final EditText text = view.findViewById(R.id.txtNumero);
        text.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                boolean flag = true;
                String eachBlock[] = text.getText().toString().split("-");

                for (int i = 0; i < eachBlock.length; i++) {
                    if (eachBlock[i].length() > 4) {
                        flag = false;
                    }
                }

                if (flag) {

                    text.setOnKeyListener(new View.OnKeyListener() {

                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {

                            if (keyCode == KeyEvent.KEYCODE_DEL)
                                keyDel = 1;
                            return false;
                        }
                    });

                    if (keyDel == 0) {

                        if (((text.getText().length() + 1) % 5) == 0) {

                            if (text.getText().toString().split("-").length <= 3) {
                                text.setText(text.getText() + "-");
                                text.setSelection(text.getText().length());
                            }
                        }
                        a = text.getText().toString();
                    } else {
                        a = text.getText().toString();
                        keyDel = 0;
                    }

                } else {
                    text.setText(a);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        });
    }

    @Override
    public void onSuccess(final opOperationResult result)
    {
        pDialog.dismiss();
        ((NuevoServicioActivity) getActivity()).getWizard().navigateNext();
    }

    @Override
    public void onCommunicationError(final opServiceUnavailableException error)
    {
        pDialog.dismiss();
        MyDynamicToast.errorMessage(getActivity(), "Tuvimos un problema con tu tarjeta, intenta nuevamente.");
    }

    @Override
    public void onError(final opOpenpayServiceException error)
    {
        int desc = 0;
        String msg = null;
        switch (error.getErrorCode()) {
            case 3001:
                desc = R.string.declined;
                msg = this.getString(desc);
                break;
            case 3002:
                desc = R.string.expired;
                msg = this.getString(desc);
                break;
            case 3003:
                desc = R.string.insufficient_funds;
                msg = this.getString(desc);
                break;
            case 3004:
                desc = R.string.stolen_card;
                msg = this.getString(desc);
                break;
            case 3005:
                desc = R.string.suspected_fraud;
                msg = this.getString(desc);
                break;

            case 2002:
                desc = R.string.already_exists;
                msg = this.getString(desc);
                break;
            default:
                desc = R.string.error_creating_card;
                msg = error.getDescription();
        }
        pDialog.dismiss();
        MyDynamicToast.errorMessage(getActivity(), "Tuvimos un problema con tu tarjeta (" + msg + ")");

    }
}
