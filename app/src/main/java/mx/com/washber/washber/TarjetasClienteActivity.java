package mx.com.washber.washber;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.view.View.OnKeyListener;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;

import mx.com.washber.washber.Adapters.ad_genericcombo;
import mx.com.washber.washber.Adapters.ad_tarjetascliente;
import mx.com.washber.washber.Clases.cl_Funciones;
import mx.com.washber.washber.Clases.cl_GenericItem;
import mx.com.washber.washber.Clases.cl_Global;
import mx.com.washber.washber.Clases.cl_ServicioH;
import mx.com.washber.washber.Clases.cl_Tarjeta;
import mx.com.washber.washber.Clases.cl_TarjetaCliente;
import mx.com.washber.washber.Clases.cl_TarjetasCliente;
import mx.com.washber.washber.WebService.ws_GenericIntRequest;
import mx.com.washber.washber.WebService.ws_GenericResult;
import mx.com.washber.washber.WebService.ws_TarjetaClienteResult;
import mx.com.washber.washber.WebService.ws_TarjetaRequest;
import mx.com.washber.washber.WebService.ws_TarjetasClienteResult;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TarjetasClienteActivity extends AppCompatActivity {

    ListView listView;
    ad_tarjetascliente adapter;
    cl_TarjetaCliente oTarjeta;
    String a;
    int keyDel;
    ArrayList<cl_GenericItem> Anios;
    ArrayList<cl_GenericItem> Meses;
    ArrayList<cl_GenericItem> Tipos;
    cl_GenericItem AnioSeleccion;
    cl_GenericItem MesSeleccion;
    cl_GenericItem TipoSeleccion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tarjetas_cliente);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Tus tarjetas");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = findViewById(R.id.lstTarjetas);
        adapter = new ad_tarjetascliente(this, cl_Global.gblTarjetas.Tarjetas);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                oTarjeta = (cl_TarjetaCliente) listView.getItemAtPosition(i);
                ConfirmDeleteTarjeta();
            }
        });

        listView.setAdapter(adapter);

        Button cmdNuevo = findViewById(R.id.cmdNuevaTarjeta);

        cmdNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {NuevaTarjeta();}
        });

    }

    public void ConfirmDeleteTarjeta()
    {
        final Dialog dialog  = new Dialog(this);
        dialog.setContentView(R.layout.layout_modalconfirmacion);

        TextView mensaje = dialog.findViewById(R.id.lblMensaje);
        mensaje.setText("¿ Deseas eliminar tu tarjeta " + oTarjeta.NumeroTarjeta + " ?");

        Button cmdCancelar = dialog.findViewById(R.id.cmdCancelar);
        Button cmdAceptar = dialog.findViewById(R.id.cmdAceptar);

        cmdCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        cmdAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteTarjeta dt = new DeleteTarjeta(oTarjeta);
                dialog.dismiss();
                dt.execute();
            }
        });

        dialog.show();
    }

    class DeleteTarjeta extends AsyncTask<Void,Void,Boolean>
    {
        final cl_TarjetaCliente oTarjeta;
        private ProgressDialog pDialog;
        private String result;

        DeleteTarjeta(cl_TarjetaCliente Tarjeta)
        {
            this.oTarjeta = Tarjeta;
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(TarjetasClienteActivity.this);
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Procesando...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = cl_Global.API_URL + cl_Global.API_DELETETARJETAC;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_GenericIntRequest oRequest = new ws_GenericIntRequest();

            try
            {
                oRequest.Valor = oTarjeta.IdTarjetaCliente;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            try
            {
                ws_GenericResult oResult = new Gson().fromJson(String.valueOf(result),ws_GenericResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        pDialog.dismiss();
                        MyDynamicToast.successMessage(TarjetasClienteActivity.this, "Tu tarjeta fue eliminada.");
                        RefreshTarjetas();
                    }
                    else if(oResult.Codigo == 101)
                    {
                        MyDynamicToast.errorMessage(TarjetasClienteActivity.this, "Tenemos un problema con la información, reintenta mas tarde.");
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.warningMessage(TarjetasClienteActivity.this, oResult.Mensaje);
                        pDialog.dismiss();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                pDialog.dismiss();
            }
        }

    }

    public void RefreshTarjetas()
    {
        getTarjetas gT = new getTarjetas(cl_Global.gblUsuario.IdCliente);
        gT.execute();
    }

    class getTarjetas extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private final Integer pIdUsuario;
        private String result;

        getTarjetas (Integer IdUsuario) {
            this.pIdUsuario = IdUsuario;
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(TarjetasClienteActivity.this);
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Obteniendo tarjetas...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = cl_Global.API_URL + cl_Global.API_TARJETASC;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_GenericIntRequest oRequest = new ws_GenericIntRequest();

            try
            {
                oRequest.Valor = this.pIdUsuario;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            cl_TarjetaCliente oTarjeta;
            cl_Tarjeta oTarjetaUsuario;
            try
            {
                ws_TarjetasClienteResult oResult = new Gson().fromJson(String.valueOf(result),ws_TarjetasClienteResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        cl_Global.gblTarjetas = new cl_TarjetasCliente();
                        cl_Global.gblTarjetas.Tarjetas = new ArrayList<cl_TarjetaCliente>();
                        cl_Global.gblUsuario.Tarjetas = new ArrayList<cl_Tarjeta>();

                        for(int i= 0; i < oResult.Tarjetas.size(); i++)
                        {
                            ws_TarjetaClienteResult t = oResult.Tarjetas.get(i);

                            oTarjetaUsuario = new cl_Tarjeta();
                            oTarjeta = new cl_TarjetaCliente();
                            oTarjeta.IdTarjetaCliente = t.IdTarjetaCliente;
                            oTarjetaUsuario.IdTarjeta = t.IdTarjetaCliente;
                            oTarjeta.NumeroTarjeta = t.NumeroTarjeta;
                            oTarjetaUsuario.Numero = t.NumeroTarjeta;

                            oTarjeta.Tipo = t.Tipo;
                            oTarjeta.Vencimiento = t.Vencimiento;
                            if(t.Tipo == 1)
                            {
                                oTarjeta.TipoNombre = "VISA";
                            }
                            else if(t.Tipo == 2)
                            {
                                oTarjeta.TipoNombre = "MASTERCARD";
                            }
                            else if(t.Tipo == 3)
                            {
                                oTarjeta.TipoNombre = "AMERICAN EXPRESS";
                            }
                            else
                            {
                                oTarjeta.TipoNombre = "NO IDENTIFICADA";
                            }

                            cl_Global.gblTarjetas.Tarjetas.add(oTarjeta);
                            cl_Global.gblUsuario.Tarjetas.add(oTarjetaUsuario);
                        }

                        pDialog.dismiss();
                        DisplayTarjetas();
                    }
                    else if(oResult.Codigo == 101)
                    {
                        MyDynamicToast.errorMessage(TarjetasClienteActivity.this, "Tenemos un problema con la información, reintenta mas tarde.");
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.warningMessage(TarjetasClienteActivity.this, oResult.Mensaje);
                        pDialog.dismiss();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                pDialog.dismiss();
            }
        }
    }

    public void DisplayTarjetas()
    {
        adapter = new ad_tarjetascliente(this, cl_Global.gblTarjetas.Tarjetas);
        listView.setAdapter(adapter);
    }

    public void NuevaTarjeta()
    {
        ad_genericcombo adGeneric;
        ArrayList<cl_GenericItem> Items;
        cl_GenericItem Item;
        Integer anio;
        cl_Funciones funciones;

        final Dialog dialog  = new Dialog(this);
        dialog.setContentView(R.layout.layout_modaladdtarjeta);

        Button cmdCancelar = dialog.findViewById(R.id.cmdCancelar);
        Button cmdAceptar = dialog.findViewById(R.id.cmdAceptar);

        cmdCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        cmdAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ValidateData(dialog);
            }
        });

        Spinner combo = dialog.findViewById(R.id.cmbTipos);
        //LLENAMOS LISTA DE TIPOS
        Items = new ArrayList<cl_GenericItem>();

        Item = new cl_GenericItem();
        Item.Valor = 1;
        Item.Cadena = "VISA";
        Items.add(Item);

        Item = new cl_GenericItem();
        Item.Valor = 2;
        Item.Cadena = "MASTERCARD";
        Items.add(Item);

        Item = new cl_GenericItem();
        Item.Valor = 3;
        Item.Cadena = "AMERICAN EXPRESS";
        Items.add(Item);

        adGeneric = new ad_genericcombo(TarjetasClienteActivity.this,Items);
        combo.setAdapter(adGeneric);
        Tipos = new ArrayList<cl_GenericItem>();
        Tipos = Items;
        combo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                TipoSeleccion = Tipos.get(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){    }

        });

        //LLENAMOS AÑOS
        Items = new ArrayList<cl_GenericItem>();
        anio = Calendar.getInstance().get(Calendar.YEAR) - 1;

        for(Integer i = 0;i < 6; i++)
        {
            anio = anio + 1;
            Item = new cl_GenericItem();
            Item.Valor = anio;
            Item.Cadena = anio.toString();
            Items.add(Item);
        }
        combo = dialog.findViewById(R.id.cmbAnio);
        adGeneric = new ad_genericcombo(TarjetasClienteActivity.this,Items);
        combo.setAdapter(adGeneric);
        Anios = new ArrayList<cl_GenericItem>();
        Anios = Items;
        combo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                AnioSeleccion = Anios.get(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){    }

        });


        //LLENAMOS MESES
        funciones = new cl_Funciones(TarjetasClienteActivity.this);
        Items = new ArrayList<cl_GenericItem>();
        Items = funciones.ListaMeses();

        combo = dialog.findViewById(R.id.cmbMes);
        adGeneric = new ad_genericcombo(TarjetasClienteActivity.this,Items);
        combo.setAdapter(adGeneric);
        Meses = new ArrayList<cl_GenericItem>();
        Meses = Items;
        combo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                MesSeleccion = Meses.get(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){    }

        });

        //EVENTOS PARA TEXTO DE NÜMERO DE TARJETA
        final EditText text = dialog.findViewById(R.id.txtNumero);
        text.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                boolean flag = true;
                String eachBlock[] = text.getText().toString().split("-");

                for (int i = 0; i < eachBlock.length; i++) {
                    if (eachBlock[i].length() > 4) {
                        flag = false;
                    }
                }

                if (flag) {

                    text.setOnKeyListener(new OnKeyListener() {

                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {

                            if (keyCode == KeyEvent.KEYCODE_DEL)
                                keyDel = 1;
                            return false;
                        }
                    });

                    if (keyDel == 0) {

                        if (((text.getText().length() + 1) % 5) == 0) {

                            if (text.getText().toString().split("-").length <= 3) {
                                text.setText(text.getText() + "-");
                                text.setSelection(text.getText().length());
                            }
                        }
                        a = text.getText().toString();
                    } else {
                        a = text.getText().toString();
                        keyDel = 0;
                    }

                } else {
                    text.setText(a);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        });

        dialog.show();
    }


    public void ValidateData(Dialog dialog)
    {
        EditText texto = dialog.findViewById(R.id.txtNumero);
        Integer anio = Calendar.getInstance().get(Calendar.YEAR);
        Integer mes = Calendar.getInstance().get(Calendar.MONTH) + 1;
        ws_TarjetaRequest oRequest = new ws_TarjetaRequest();

        if(texto.getText().toString().equals(""))
        {
            MyDynamicToast.warningMessage(TarjetasClienteActivity.this, "Especifica el número de tu tarjeta");
            return;
        }

        if(AnioSeleccion.Valor <= anio && MesSeleccion.Valor <= mes)
        {
            MyDynamicToast.warningMessage(TarjetasClienteActivity.this, "El vencimiento debe ser mayor al año y mes actual");
            return;
        }

        oRequest.IdCliente = cl_Global.gblUsuario.IdCliente;
        oRequest.Tipo = TipoSeleccion.Valor;
        oRequest.AnioVencimiento = AnioSeleccion.Valor;
        oRequest.MesVencimiento = MesSeleccion.Valor;
        oRequest.NumeroTarjeta = texto.getText().toString().replace("-","");
        saveTarjeta sT = new saveTarjeta(oRequest);
        dialog.dismiss();
        sT.execute();
    }

    class saveTarjeta extends AsyncTask<Void,Void, Boolean>
    {
        private ProgressDialog pDialog;
        private String result;
        private ws_TarjetaRequest oRequest = new ws_TarjetaRequest();

        saveTarjeta(ws_TarjetaRequest Request)
        {
            this.oRequest = Request;
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(TarjetasClienteActivity.this);
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Procesando...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = cl_Global.API_URL + cl_Global.API_SAVETARJETAC;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            try
            {
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            try
            {
                ws_GenericResult oResult = new Gson().fromJson(String.valueOf(result),ws_GenericResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        MyDynamicToast.successMessage(TarjetasClienteActivity.this, "Tarjeta registrada.");
                        pDialog.dismiss();
                        RefreshTarjetas();
                    }
                    else if (oResult.Codigo == 102)
                    {
                        MyDynamicToast.informationMessage(TarjetasClienteActivity.this, oResult.Mensaje);
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.errorMessage(TarjetasClienteActivity.this, "Tuvimos un problema al registrar tu tarjeta, intenta nuevamente.");
                        pDialog.dismiss();
                    }
                }
            }
            catch (Exception ex)
            {
                MyDynamicToast.errorMessage(TarjetasClienteActivity.this, "Tuvimos un problema al registrar la tarjeta, intenta nuevamente.");
            }
        }
    }
}
