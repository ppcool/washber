package mx.com.washber.washber;

import android.app.Activity;
import android.app.ProgressDialog;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.content.DialogInterface;
import android.widget.EditText;


import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.google.gson.Gson;

import java.util.ArrayList;

import mx.com.washber.washber.Clases.cl_Global;
import mx.com.washber.washber.Clases.cl_Tarjeta;
import mx.com.washber.washber.Clases.cl_Usuario;
import mx.com.washber.washber.WebService.ws_LoginRequest;
import mx.com.washber.washber.WebService.ws_LoginResult;
import mx.com.washber.washber.WebService.ws_TarjetaResult;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;




public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Register(View view)
    {
        Intent intento = new Intent(this,RegistroActivity.class);
        startActivity(intento);
    }

    public void LogIn(View view)
    {
        EditText login = findViewById(R.id.txtLogin);
        EditText password = findViewById(R.id.txtPassword);
        if(login.getText().toString().trim().length() == 0)
        {
            MyDynamicToast.warningMessage(MainActivity.this, "Especifica tu usuario");
            return;
        }

        if(password.getText().toString().trim().length() == 0)
        {
            MyDynamicToast.warningMessage(MainActivity.this, "Especifica tu password");
            return;
        }

        functionLogin fLogin = new functionLogin(login.getText().toString(),password.getText().toString());
        fLogin.execute();
    }

    public void Ingresa()
    {
        Intent intent = new Intent(this,PrincipalActivity.class);
        startActivity(intent);
        finish();
    }

    class functionLogin extends AsyncTask<Void,Void,Boolean> {
        private ProgressDialog pDialog;
        private final String pUser;
        private final String pPassword;
        private String result;

        functionLogin(String user,String psw)
        {
            pUser = user.trim();
            pPassword = psw.trim();
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Procesando...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            String url = cl_Global.API_URL + cl_Global.API_LOGIN;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_LoginRequest requestLogin = new ws_LoginRequest();
            try {
                requestLogin.Login = pUser;
                requestLogin.Password = pPassword;
                String jsRequest = h.toJson(requestLogin);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;

            }
        }

        protected void onPostExecute(final Boolean success) {
            ws_TarjetaResult oTarjetaResult;
            cl_Tarjeta oTarjeta;
            try
            {
                ws_LoginResult lResult = new Gson().fromJson(String.valueOf(result),ws_LoginResult.class);
                if(success)
                {
                    if(lResult.Codigo == 100)
                    {
                        cl_Global.gblUsuario = new cl_Usuario();
                        cl_Global.gblUsuario.IdCliente = lResult.IdCliente;
                        cl_Global.gblUsuario.Nombre = lResult.Nombre;
                        cl_Global.gblUsuario.Fotografia = lResult.Fotografia;
                        cl_Global.gblUsuario.DiasServicio = lResult.DiasServicio;
                        cl_Global.gblUsuario.Pendientes = lResult.Pendientes;
                        cl_Global.gblUsuario.Proceso = lResult.Proceso;
                        cl_Global.gblUsuario.Cerrados = lResult.Cerrados;
                        cl_Global.gblUsuario.IVA = lResult.IVA;
                        cl_Global.gblUsuario.Cancelados = lResult.Cancelados;
                        cl_Global.gblUsuario.MontoMinimoServicio = lResult.MontoMinimoServicio;
                        cl_Global.OpenPayProduccion = lResult.OpenPayProduction;

                        pDialog.dismiss();

                        Ingresa();
                    }
                    else if(lResult.Codigo == 102)
                    {
                        MyDynamicToast.informationMessage(MainActivity.this, lResult.Mensaje);
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.errorMessage(MainActivity.this, "Tuvimos un problema con la información intenta mas tarde.");
                        pDialog.dismiss();
                    }
                }
                else
                {
                    pDialog.dismiss();
                    MyDynamicToast.errorMessage(MainActivity.this, "Tuvimos un problema con la información intenta mas tarde.");
                }

            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }
}
