package mx.com.washber.washber.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mx.com.washber.washber.Clases.cl_GenericItem;
import mx.com.washber.washber.R;

/**
 * Created by ppcoo on 24/01/2018.
 */

public class ad_genericcombo extends BaseAdapter {

    Context context;
    LayoutInflater inflter;
    ArrayList<cl_GenericItem> oItems;

    public ad_genericcombo(Context applicationContext,ArrayList<cl_GenericItem> Items)
    {
        this.context = applicationContext;
        inflter = (LayoutInflater.from(applicationContext));
        this.oItems = Items;
    }

    @Override
    public int getCount() {return oItems.size();}

    @Override
    public Object getItem(int i) {return null;}

    @Override
    public long getItemId(int i) {return 0;}

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        view = inflter.inflate(R.layout.layout_genericcomboitem, null);
        cl_GenericItem oItem = oItems.get(i);
        TextView Texto = view.findViewById(R.id.lblTexto);
        Texto.setText(oItem.Cadena);

        return view;
    }
}
