package mx.com.washber.washber.OpenPay;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.HttpUnsuccessfulResponseHandler;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;

import java.io.IOException;

/**
 * Created by ppcoo on 16/02/2018.
 */

public abstract class opBaseService<V, T> {

    private final static JsonFactory JSON_FACTORY = new JacksonFactory();
    private final static HttpTransport HTTP_TRANSPORT = AndroidHttp.newCompatibleTransport();
    private final static String EMPTY_PASSWORD = "";
    private final static String API_VERSION = "v1";
    private final static String URL_SEPARATOR = "/";
    private static final String AGENT = "openpay-android/";

    private static final int DEFAULT_CONNECTION_TIMEOUT = 60000;

    protected String baseUrl;
    protected String merchantId;
    protected String apiKey;

    protected Class<T> clazz;

    public opBaseService(final String baseUrl, final String merchantId, final String apiKey, final Class<T> clazz) {
        this.merchantId = merchantId;
        this.apiKey = apiKey;
        this.clazz = clazz;
        this.baseUrl = baseUrl;
    }

    public HttpRequestFactory getRequestFactory() {
        return HTTP_TRANSPORT.createRequestFactory(new OpenpayHttpRequestInitializer());
    }

    public GenericUrl getGenericUrl(final String resourceUrl) {
        StringBuilder urlBuilder = new StringBuilder(this.baseUrl).append(URL_SEPARATOR).append(API_VERSION)
                .append(URL_SEPARATOR).append(this.merchantId).append(URL_SEPARATOR)
                .append(resourceUrl);
        return new GenericUrl(urlBuilder.toString());
    }

    public HttpContent getContent(final V jsonData) {
        return new JsonHttpContent(JSON_FACTORY, jsonData);
    }

    public T post(final String resourceUrl, final V data) throws opOpenpayServiceException,
            opServiceUnavailableException {
        try {
            HttpRequest request = this.getRequestFactory().buildPostRequest(this.getGenericUrl(resourceUrl),
                    this.getContent(data));
            T newObject = request.execute().parseAs(this.clazz);
            return newObject;
        } catch (IOException e) {
            if (e instanceof opOpenpayServiceException) {
                throw (opOpenpayServiceException) e;
            }
            throw new opServiceUnavailableException(e);
        }
    }

    private class OpenpayHttpRequestInitializer implements HttpRequestInitializer {
        @Override
        public void initialize(final HttpRequest request) {
            request.setParser(new JsonObjectParser(JSON_FACTORY));
            request.getHeaders().setBasicAuthentication(opBaseService.this.apiKey, EMPTY_PASSWORD);
            String version = this.getClass().getPackage().getImplementationVersion();
            if (version == null) {
                version = "1.0.1-UNKNOWN";
            }
            request.setSuppressUserAgentSuffix(true);
            request.getHeaders().setUserAgent(AGENT + version);
            request.setUnsuccessfulResponseHandler(new ServiceUnsuccessfulResponseHandler());
            request.setConnectTimeout(DEFAULT_CONNECTION_TIMEOUT);
            request.setReadTimeout(DEFAULT_CONNECTION_TIMEOUT);
        }

        private class ServiceUnsuccessfulResponseHandler implements HttpUnsuccessfulResponseHandler {
            @Override
            public boolean handleResponse(final HttpRequest request, final HttpResponse response, final boolean arg2)
                    throws IOException {
                opOpenpayServiceException exception = new JsonObjectParser(opBaseService.JSON_FACTORY).parseAndClose(
                        response.getContent(), response.getContentCharset(), opOpenpayServiceException.class);
                throw exception;
            }

        }
    }

}
