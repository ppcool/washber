package mx.com.washber.washber;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;


import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.kofigyan.stateprogressbar.StateProgressBar;
import com.shawnlin.numberpicker.NumberPicker;

import java.text.NumberFormat;
import java.util.ArrayList;

import mx.com.washber.washber.Adapters.ad_categoriacombo;
import mx.com.washber.washber.Adapters.ad_prendaitem;
import mx.com.washber.washber.Adapters.ad_subcategoriacombo;
import mx.com.washber.washber.Clases.cl_Categoria;
import mx.com.washber.washber.Clases.cl_Global;
import mx.com.washber.washber.Clases.cl_Prenda;
import mx.com.washber.washber.Clases.cl_Proveedor;
import mx.com.washber.washber.Clases.cl_Subcategoria;


/**
 * A simple {@link Fragment} subclass.
 */
public class PrendasFragment extends Fragment {

    private Button goNextButton;
    private Button goBackButton;
    private Button cancelButton;
    private Button agregarButton;
    private NumberPicker npPiezas;

    private cl_Proveedor oProveedor;
    ListView lvPrendas;
    private ad_prendaitem aPrendas;
    private cl_Prenda oPrenda;
    private Spinner cmbcategorias;
    private ad_categoriacombo aCategorias;
    private ArrayList<cl_Subcategoria> oSubcategorias;
    private cl_Categoria oCategoria;
    private Spinner cmbsubcategorias;
    private cl_Subcategoria oSubcategoria;

    public PrendasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_prendas, container, false);
        View view = inflater.inflate(R.layout.fragment_prendas, container, false);

        //TOOLBAR
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Prendas");

        //BARRA DE PROGRESO
        StateProgressBar stateProgressBar = view.findViewById(R.id.pgrBar);
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);

        //BOTONES
        goNextButton = view.findViewById(R.id.goNextButton);
        goNextButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                DataValidation();
            }
        });

        goBackButton = view.findViewById(R.id.goBackButton);
        goBackButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ((NuevoServicioActivity) getActivity()).getWizard().navigatePrevious();
            }
        });

        cancelButton = view.findViewById(R.id.cmdCancelar);
        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                Cancel();
            }
        });

        agregarButton = view.findViewById(R.id.cmdNuevaPrenda);
        agregarButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                AddPrenda();
            }
        });

        oProveedor = cl_Global.gblNuevoServicio.Proveedor;

        //LISTA DE PRENDAS
        lvPrendas = view.findViewById(R.id.lstPrendas);
        if(cl_Global.gblNuevoServicio.Prendas == null)
        {
            cl_Global.gblNuevoServicio.Prendas = new ArrayList<cl_Prenda>();
        }

        //EVENTO DE LISTA DE PRENDAS

        aPrendas = new ad_prendaitem(getContext(),cl_Global.gblNuevoServicio.Prendas);
        lvPrendas.setAdapter(aPrendas);
        lvPrendas.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                oPrenda = cl_Global.gblNuevoServicio.Prendas.get(i);
                DeletePrenda();
            }
        });

        return  view;
    }

    public void  Cancel()
    {
        Intent intent = new Intent(getActivity(),PrincipalActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    public void DisplayDetail()
    {

    }

    public void AddPrenda()
    {
        final Dialog dialog  = new Dialog(getActivity());
        dialog.setContentView(R.layout.layout_modaladdprenda);
        dialog.setTitle("Agregar prenda.");
        oPrenda = new cl_Prenda();

        //ETIQUETA DE PRECIO
        final TextView lblprecio = dialog.findViewById(R.id.lblPrecio);

        //EVENTO DE BOTÓN DE CANCELACIÓN
        Button Cancelar = dialog.findViewById(R.id.cmdCancelar);
        Cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //BOTON ACEPTAR
        Button Aceptar = dialog.findViewById(R.id.cmdAceptar);

        //SPINNER DE CATEGORIAS Y SUBCATEGORIAS
        cmbcategorias = dialog.findViewById(R.id.cmbCategorias);
        cmbsubcategorias = dialog.findViewById(R.id.cmbSubcategorias);
        cmbcategorias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            private ad_subcategoriacombo aSubcategorias;

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                //Fitramos subcategorias de acuerdo a la categoría seleccionada
                oCategoria = cl_Global.gblNuevoServicio.Proveedor.Categorias.get(pos);
                oSubcategorias = new ArrayList<cl_Subcategoria>();
                for(Integer s=0; s< oCategoria.Subcategorias.size();s++)
                {
                    oSubcategorias.add(oCategoria.Subcategorias.get(s));
                }


                aSubcategorias = new ad_subcategoriacombo(getContext(),oSubcategorias);
                cmbsubcategorias.setAdapter(aSubcategorias);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){    }
        });

        cmbsubcategorias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                oSubcategoria = oSubcategorias.get(pos);
                lblprecio.setText("Precio por pieza: " + oSubcategoria.CadenaPrecio);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){    }
        });

        aCategorias = new ad_categoriacombo(getContext(),cl_Global.gblNuevoServicio.Proveedor.Categorias);
        cmbcategorias.setAdapter(aCategorias);

        //NUMERO DE PIEZAS
        npPiezas = dialog.findViewById(R.id.txtNumeroPiezas);
        npPiezas.setValue(1);

        //EVENTO ACEPTAR
        Aceptar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                NewPrenda();
                dialog.dismiss();
            }
        });
        dialog.setCancelable(false);
        dialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void NewPrenda()
    {
        oPrenda = new cl_Prenda();
        oPrenda.IdSubcategoria = oSubcategoria.IdSubcategoria;
        oPrenda.CadenaPrecio = oSubcategoria.CadenaPrecio;
        oPrenda.Precio = oSubcategoria.Precio;
        oPrenda.Piezas = npPiezas.getValue();
        oPrenda.Imagen = oSubcategoria.Imagen;
        oPrenda.Categoria = oCategoria.Nombre;
        oPrenda.Subcategoria = oSubcategoria.Nombre;

        if(cl_Global.gblNuevoServicio.Prendas == null)
        {
            cl_Global.gblNuevoServicio.Prendas =new  ArrayList<cl_Prenda>();
        }

        cl_Global.gblNuevoServicio.Prendas.add(oPrenda);
        aPrendas = new ad_prendaitem(getContext(),cl_Global.gblNuevoServicio.Prendas);
        lvPrendas.setAdapter(aPrendas);
    }

    private void DeletePrenda()
    {

        final Dialog dialog  = new Dialog(getActivity());
        dialog.setContentView(R.layout.layout_modalconfirmacion);

        TextView mensaje = dialog.findViewById(R.id.lblMensaje);
        mensaje.setText("¿ Deseas eliminar la prenda del servicio ?");

        Button cmdCancelar = dialog.findViewById(R.id.cmdCancelar);
        Button cmdAceptar = dialog.findViewById(R.id.cmdAceptar);

        cmdCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        cmdAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cl_Global.gblNuevoServicio.Prendas.remove(oPrenda);
                aPrendas = new ad_prendaitem(getContext(),cl_Global.gblNuevoServicio.Prendas);
                lvPrendas.setAdapter(aPrendas);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void DataValidation()
    {
        float subTotal;
        float iva;
        cl_Prenda prenda;
        NumberFormat format = NumberFormat.getCurrencyInstance();

        if(cl_Global.gblNuevoServicio.Prendas == null)
        {
            MyDynamicToast.warningMessage(getActivity(), "Especifica las prendas del servicio");
            return;
        }
        else if(cl_Global.gblNuevoServicio.Prendas.size() == 0)
        {
            MyDynamicToast.warningMessage(getActivity(), "Especifica las prendas del servicio");
            return;
        }
        else {
            //Validamos el total

            subTotal = 0;
            for(Integer i =0 ; i< cl_Global.gblNuevoServicio.Prendas.size(); i++)
            {
                prenda = cl_Global.gblNuevoServicio.Prendas.get(i);
                subTotal += prenda.Precio * prenda.Piezas;
            }

            if(subTotal < cl_Global.gblUsuario.MontoMinimoServicio)
            {
                MyDynamicToast.warningMessage(getActivity(), "Te recordamos que el monto mínimo es de " + format.format(cl_Global.gblUsuario.MontoMinimoServicio));
                return;
            }

            ((NuevoServicioActivity) getActivity()).getWizard().navigateNext();
        }
    }
}
