package mx.com.washber.washber.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mx.com.washber.washber.Clases.cl_ServicioH;
import mx.com.washber.washber.R;

/**
 * Created by ppcoo on 26/12/2017.
 */

public class ad_servicioheader extends ArrayAdapter<cl_ServicioH> {

    public ad_servicioheader (Context context, ArrayList<cl_ServicioH> elementos)
    {
        super(context,0,elementos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        cl_ServicioH elemento = getItem(position);

        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_servicioheader,parent,false);
        }

        TextView Folio = convertView.findViewById(R.id.txtFolio);
        TextView Fecha = convertView.findViewById(R.id.txtFecha);
        TextView Estatus = convertView.findViewById(R.id.txtEstatus);

        Folio.setText("Folio:  "+ elemento.Folio);
        Fecha.setText("Registrado: " + elemento.Fecha);
        Estatus.setText(elemento.Estatus);

        return convertView;
    }
}
