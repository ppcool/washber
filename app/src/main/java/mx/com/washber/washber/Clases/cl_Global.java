package mx.com.washber.washber.Clases;


import java.util.ArrayList;

import mx.com.washber.washber.OpenPay.opCard;

/**
 * Created by ppcoo on 26/12/2017.
 */

public class cl_Global {
    private static cl_Global instance;

    //------------------------------------ VARIABLES WEBSERVICE
    public static String API_URL = "https://www.washber.com.mx/Servicio/wbServicio.svc";
    public static String API_URLDESARROLLO = "http://washberdev.azurewebsites.net/Servicio/wbServicio.svc";
    public static String API_LOGIN = "/CLogin";
    public static String API_REGISTER = "/CRegistro";
    public static String API_SERVICIOS = "/CServicios";
    public static String API_SERVICIOSPROCESS = "/CServiciosHeaderProceso";
    public static String API_SERVICIODETALLE = "/SDetalle";
    public static String API_KARDEX = "/SKardex";
    public static String API_PROVEEDORESCERCANIA = "/PCercania";
    public static String API_GUARDASERVICIO = "/SGuarda";
    public static String API_TARJETASC = "/CTarjetas";
    public static String API_SAVETARJETAC = "/CGuardaTarjeta";
    public static String API_DELETETARJETAC = "/CEliminaTarjeta";


    //------------------------------------ VARIABLES DE USUARIO
    public static cl_Usuario gblUsuario;
    public static cl_ServiciosH gblServiciosHeader;
    public static cl_Kardex gblKardex;
    public static cl_Proveedores gblProveedores;
    public static cl_NuevoServicio gblNuevoServicio;
    public static cl_TarjetasCliente gblTarjetas;

    //------------------------------------ VARIABLES OPENPAY

    public static String URL_OPENPAY = "https://sandbox-api.openpay.mx";
    public static String OpenPayID = "mscjzbm7pqdozlbexwga";
    public static String OpenPayPrivada = "sk_61a76c0b660648ad95e88ca9d5ba6450";
    public static  String OpenPayPublica = "pk_022dc2b795904af789660e002ba0ddf1";
    public static boolean OpenPayProduccion = false;
    public static String gblCardToken;
    public static String gblDeviceID;
    public static String gblStrTarjeta;
    public static opCard gblTarjeta;
}
