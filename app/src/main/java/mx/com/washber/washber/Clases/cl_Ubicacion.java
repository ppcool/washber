package mx.com.washber.washber.Clases;

import me.panavtec.wizard.WizardPage;
import mx.com.washber.washber.UbicacionFragment;


/**
 * Created by ppcoo on 29/12/2017.
 */

public class cl_Ubicacion extends WizardPage<UbicacionFragment> {

    @Override
    public UbicacionFragment createFragment()
    {
        return new UbicacionFragment();
    }
}
