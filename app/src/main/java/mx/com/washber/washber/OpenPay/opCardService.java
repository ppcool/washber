package mx.com.washber.washber.OpenPay;

/**
 * Created by ppcoo on 16/02/2018.
 */

public class opCardService extends opBaseService<opCard, opCard> {

    private static final String MERCHANT_CARDS = "cards";
    private static final String CUSTOMER_CARDS = "customers/%s/cards";

    public opCardService(final String baseUrl, final String merchantId, final String apiKey) {
        super(baseUrl, merchantId, apiKey, opCard.class);
    }

    public opCard create(final opCard card, final String customerId) throws opOpenpayServiceException,
            opServiceUnavailableException {
        String resourceUrl = MERCHANT_CARDS;
        if (customerId != null && !customerId.equals("")) {
            resourceUrl = String.format(CUSTOMER_CARDS, customerId);
        }

        return this.post(resourceUrl, card);
    }
}
