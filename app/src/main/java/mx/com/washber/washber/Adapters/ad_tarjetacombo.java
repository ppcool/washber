package mx.com.washber.washber.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mx.com.washber.washber.Clases.cl_Tarjeta;
import mx.com.washber.washber.R;

/**
 * Created by ppcoo on 07/01/2018.
 */

public class ad_tarjetacombo extends BaseAdapter {

    Context context;
    LayoutInflater inflter;
    ArrayList<cl_Tarjeta> oTarjetas;

    public ad_tarjetacombo (Context applicationContext,ArrayList<cl_Tarjeta> Tarjetas)
    {
        this.context = applicationContext;
        inflter = (LayoutInflater.from(applicationContext));
        oTarjetas = Tarjetas;
    }

    @Override
    public int getCount() {return oTarjetas.size();}

    @Override
    public Object getItem(int i) {return null;}

    @Override
    public long getItemId(int i) {return 0;}

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        view = inflter.inflate(R.layout.layout_genericcomboitem, null);
        cl_Tarjeta oTarjeta = oTarjetas.get(i);
        TextView Texto = view.findViewById(R.id.lblTexto);
        Texto.setText(oTarjeta.Numero);

        return view;
    }
}
