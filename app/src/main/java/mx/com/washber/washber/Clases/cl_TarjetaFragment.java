package mx.com.washber.washber.Clases;

import me.panavtec.wizard.WizardPage;
import mx.com.washber.washber.TarjetaFragment;

/**
 * Created by ppcoo on 19/02/2018.
 */

public class cl_TarjetaFragment extends WizardPage<TarjetaFragment> {

    @Override
    public TarjetaFragment createFragment()
    {
        return new TarjetaFragment();
    }
}
